﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode,System.Serializable]
public class PillarSetup : MonoBehaviour {

    #region Variables
    /// <summary>
    /// Pillar height
    /// </summary>
    [SerializeField]
    private int _pillarLevel;

    /// <summary>
    /// y-pos of the pillar relative to the pillar level
    /// </summary>
    private float _pillarYPos
    {
        get
        {
            return _pillarLevel * 3f;
        }
    }

    /// <summary>
    /// Pillar's local position
    /// </summary>
    private Vector2 _pillarLocalPos;

    /// <summary>
    /// Pillar height
    /// </summary>
    [SerializeField]
    public int pillarLevel
    {
        get
        {
            return _pillarLevel;
        }
    }
    #endregion

    #region Functions

    /// <summary>
    /// Raise the height of the pillar
    /// </summary>
    public void raise()
    {
        _pillarLevel++;
    }

    /// <summary>
    /// Lower the height of the pillar
    /// </summary>
    public void lower()
    {
        if (pillarLevel > 0)
        {
            _pillarLevel--;
        }
    }

    /// <summary>
    /// Reset the pillar height
    /// </summary>
    public void resetHeight()
    {
        _pillarLevel = 0;
    }
    
    /// <summary>
    /// Move the pillar to the set height
    /// </summary>
    /// <param name="aStartDelay">Delay till the pillar moves</param>
    /// <param name="aSlideIntoPlace">Do we want the pillar to slide into place?</param>
    public void moveToSetHeight(float aStartDelay = 0, bool aSlideIntoPlace = false)
    {
        //If the game is running, and we want to slide it into place
        if (Application.isPlaying && aSlideIntoPlace)
        {
            //Get the pillar local position
            _pillarLocalPos = new Vector2(transform.localPosition.x, _pillarYPos);

            //If the pillar is not is the location we want to move to
            if ((Vector2)transform.localPosition != _pillarLocalPos)
            {
                //Move it to that position
                StartCoroutine(ImoveToNewHeight(aStartDelay));
            }
        }
        else
        {
            //Move it to the designated position
            transform.localPosition = new Vector2(transform.localPosition.x, _pillarYPos);

            //Set the arena to a dirty state
            ArenaSetup.instance.setToDirty();
        }
    }
    
    #endregion

    #region Coroutines

    /// <summary>
    /// Coroutine to move the pillar into position
    /// </summary>
    /// <param name="aStartDelay">Delay before moving to postion</param>
    /// <returns></returns>
    IEnumerator ImoveToNewHeight(float aStartDelay)
    {
        float time = 0;

        //Get the starting position, and end position of the pillars
        Vector2 startPos = transform.localPosition;
        Vector2 endPos = _pillarLocalPos;

        //Delay the movement of the pillar
        yield return new WaitForSeconds(aStartDelay);

        //Move the pillar into position
        while (time < 1)
        {
            time += Time.deltaTime;

            transform.localPosition = Vector2.Lerp(transform.localPosition, endPos, time);

            yield return new WaitForEndOfFrame();
        }

    }

    #endregion

    #region Unity Functions

    void Start()
    {
        _pillarLevel = (int)(transform.localPosition.y / 3f);
    }

    #endregion

}
