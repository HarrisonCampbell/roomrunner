﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using ExtensionClasses.CSharp.Array;
using ExtensionClasses.GameObject;
using ExtensionClasses.Sorting;

/// <summary>
/// Handles everything that changes the arena
/// </summary>
public class ArenaSetup : MonoBehaviour
{
    #region Variables

    /// <summary>
    /// Is there an arena in the scene
    /// </summary>
    public static bool inScene
    {
        get
        {
            return FindObjectOfType<ArenaSetup>() != null;
        }
    }

    /// <summary>
    /// The current level set
    /// </summary>
    public static LevelSet<int> levelSet;

    /// <summary>
    /// Level type
    /// </summary>
    public static LevelStructure levelStruct;

    /// <summary>
    /// There can only be one instance of the Arena
    /// </summary>
    private static ArenaSetup _instance;

    /// <summary>
    /// There can only be one instance of the Arena
    /// </summary>
    public static ArenaSetup instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ArenaSetup>() as ArenaSetup;
            }

            return _instance;
        }
    }

    /// <summary>
    /// A string representation of the arena
    /// </summary>
    private string _arenaLayout
    {
        get
        {
            return _savePillarHeights() + "-" + _saveItemLayout();
        }
    }

    /// <summary>
    /// Holds information in a cell
    /// </summary>
    public struct CellDetails
    {
        /// <summary>
        /// Item type
        /// </summary>
        public Item item;

        /// <summary>
        /// Base Object
        /// </summary>
        public BaseObject baseObj;

        /// <summary>
        /// Creates a set of details for a cell
        /// </summary>
        /// <param name="aItem">Item type</param>
        /// <param name="aObj">Unit component</param>
        /// <returns>Return a CellDetails object</returns>
        public static CellDetails createCellDetails(Item aItem, BaseObject aObj)
        {
            CellDetails cellDet = new CellDetails();
            cellDet.item = aItem;
            cellDet.baseObj = aObj;

            return cellDet;
        }

        /// <summary>
        /// Creates a set of details for a cell
        /// </summary>
        /// <param name="aItem">Item type</param>
        /// <param name="aGameObject">Unit GameObject</param>
        /// <returns>Return a CellDetails object</returns>
        public static CellDetails createCellDetails(Item aItem, GameObject aGameObject)
        {
            CellDetails cellDet = new CellDetails();
            cellDet.item = aItem;
            cellDet.baseObj = aGameObject.GetComponent<BaseObject>();

            return cellDet;
        }
    }

    /// <summary>
    /// Sides of the arena
    /// </summary>
    public enum ArenaSide
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }

    /// <summary>
    /// Item types that appear in the arena
    /// </summary>
    public enum Item
    {
        ENEMY
    }

    /// <summary>
    /// Current item that is selected
    /// </summary>
    public Item itemSelected;

    /// <summary>
    /// Items placed in the arena
    /// </summary>
    private Dictionary<Vector2, CellDetails> _itemPlacements;

    /// <summary>
    /// Items placed in the arena
    /// </summary>
    public Dictionary<Vector2, CellDetails> itemPlacements
    {
        get
        {
            if (_itemPlacements == null)
            {
                _itemPlacements = new Dictionary<Vector2, CellDetails>();
            }

            return _itemPlacements;
        }
    }

    /// <summary>
    /// Is the current level being tested
    /// </summary>
    public bool testLevel;

    /// <summary>
    /// The game type that we want to test
    /// </summary>
    public GameState.GameType testGameType;

    /// <summary>
    /// Is the next level ready to be loaded
    /// </summary>
    public bool loadedNewLevel = false;

    /// <summary>
    /// Level that is loaded in the scene
    /// </summary>
    public int currentLevel = 0;

    /// <summary>
    /// Is the data in a fresh state
    /// </summary>
    public bool isDirty = false;

    #endregion

    #region Functions

    /// <summary>
    /// Thread that loads in the pillar layout
    /// </summary>
    /// <param name="aLayout">Layout that we want to load in</param>
    /// <returns>The thread that sets up the arena</returns>
    private IEnumerator _inGameLoadPillarLayout(string aLayout)
    {
        //Get all of the sides from the layout, and rotate the array to the right
        string[] sideLayout = aLayout.Split(':');
        sideLayout.rotateRight();

        //Get the gamobject that holds the arena's sides
        GameObject sidesHolder = GameObject.Find("Arena/Sides");

        //Get the array of childrens gameobjects of the arena
        GameObject[] arenaSides = sidesHolder.getChildGameObjects();
        arenaSides.rotateRight();

        //Go through each side
        for (int i = 0; i < arenaSides.Length; i++)
        {
            //Get the heaight of each pillar in the side
            string[] pillarLayout = sideLayout[i].Split(',');

            //Go through each pillar
            for (int j = 0; j < arenaSides[i].transform.childCount; j++)
            {
                //Get the pillar's behaviour
                PillarSetup pillar = arenaSides[i].transform.GetChild(j).GetComponent<PillarSetup>();

                //Get the height of the pillar for the new setup
                int newHeight = int.Parse(pillarLayout[j]);

                //If the current pillar height does not equal the new height
                if (pillar.pillarLevel != newHeight)
                {
                    //Delay the process
                    yield return new WaitForSeconds(0.15f);

                    //Reset the height of the pillar
                    pillar.resetHeight();

                    //Raise the pillars height to the new height
                    while (pillar.pillarLevel < int.Parse(pillarLayout[j]))
                    {
                        pillar.raise();
                    }

                    //If the side is the southSide and its the bottom left half of the arena
                    if (arenaSides[i].name.Equals("SouthSide") && j < arenaSides[i].transform.childCount / 2)
                    {
                        //Delay the pillar by a second and move it to the height
                        pillar.moveToSetHeight(1, true);
                    }
                    else
                    {
                        //Move it to the new height
                        pillar.moveToSetHeight(0, true);
                    }

                    //Wait till the end of the frame
                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }

    /// <summary>
    /// Loads a possible layout
    /// </summary>
    /// <param name="aLvlSelect">The selected level</param>
    public void loadNewLevel(int aLvlSelect)
    {
        //Clear items in the level
        BaseObject.destroyAll();

        //Get the layout from the level selected
        string newLevel = SaveManager.instance.allPossibleLevels[aLvlSelect];
        string[] levelSections = newLevel.Split('-');
        StartCoroutine(_inGameLoadPillarLayout(levelSections[0]));

        if (!levelSections[1].Equals("\r"))
        {
            _loadItemLayout(levelSections[1]);
        }
    }

    /// <summary>
    /// Loads a pillar layout
    /// </summary>
    /// <param name="aNewLevel">Level to load</param>
    private void _loadPillarLayout(string aNewLevel)
    {
        //Get an array of the arena sides and their pillar heights
        string[] sideLayout = aNewLevel.Split(':');

        //Get the gameobject that holds the sides
        GameObject sidesHolder = GameObject.Find("Arena/Sides");

        //Go through each side
        for (int i = 0; i < sidesHolder.transform.childCount; i++)
        {
            //Get the pillar heights for the side
            string[] pillarLayout = sideLayout[i].Split(',');

            //Get the side of the arena
            GameObject side = sidesHolder.transform.GetChild(i).gameObject;

            //Go through each pillar
            for (int j = 0; j < side.transform.childCount; j++)
            {
                //Get the pillar behaviour
                PillarSetup pillar = side.transform.GetChild(j).GetComponent<PillarSetup>();

                //Reset the height
                pillar.resetHeight();

                //Raise it to the height set
                while (pillar.pillarLevel < int.Parse(pillarLayout[j]))
                {
                    pillar.raise();
                }

                //Move the pillar to the set height
                pillar.moveToSetHeight(0, true);
            }
        }
    }

    /// <summary>
    /// Loads an item layout
    /// </summary>
    /// <param name="aNewItems">The string of items to load</param>
    private void _loadItemLayout(string aNewItems)
    {
        //Clear the item dictionary
        itemPlacements.Clear();

        //Get each item
        string[] itemLayout = aNewItems.Split(':');

        //Go through each item
        foreach (string item in itemLayout)
        {
            if (item.Length > 2)
            {
                //Get the item details
                string[] itemContents = item.Split('|');
                string[] locations = itemContents[0].Split(',');
                int xPos = int.Parse(locations[0]);
                int yPos = int.Parse(locations[1]);
                Vector2 location = new Vector2(xPos, yPos);

                string newItem = itemContents[1];
                if (newItem.Substring(newItem.Length - 2).Equals("/r"))
                {
                    newItem = newItem.Substring(0, newItem.Length - 2);
                }

                Item itemType = (Item)System.Enum.Parse(typeof(Item), newItem);

                BaseObject newObject = null;

                if (itemType == Item.ENEMY)
                {
                    newObject = BaseUnit.create((int)location.x, (int)location.y).GetComponent<BaseObject>();
                }
                else
                {
                    Debug.LogWarning("Not yet implemented");
                }

                CellDetails cellDetails = CellDetails.createCellDetails(itemType, newObject);
                //Add the item to the list
                itemPlacements.Add(location, cellDetails);
            }
        }
    }

    /// <summary>
    /// Load level from the editor
    /// </summary>
    public void loadNewLevelEditor()
    {
        //Clear items in the level
        BaseObject.destroyAll();

        //Get the new level
        string newLevel = SaveManager.instance.allPossibleLevels[currentLevel];

        string[] levelSections = newLevel.Split('-');
        _loadPillarLayout(levelSections[0]);

        if (!string.IsNullOrEmpty(levelSections[1]))
        {
            _loadItemLayout(levelSections[1]);
        }

        //Clear the item list
        itemPlacements.Clear();

        //Set the arena to clean
        setToClean();
    }

    /// <summary>
    /// Reload the current level
    /// </summary>
    public void reloadLevel()
    {
        //Reload the current level
        _loadSelectedLevel(currentLevel);
    }

    /// <summary>
    /// Load a new level
    /// </summary>
    /// <param name="aLevel">Level index that will load the designated level</param>
    /// <returns>Was the level succesfully loaded</returns>
    private void _loadSelectedLevel(int aLevel)
    {
        //Set the current level to the selected level
        currentLevel = aLevel;

        //If the application is Playing
        if (Application.isPlaying)
        {
            loadNewLevel(aLevel);
        }
        //Else if the application isn't playing
        else
        {
            //Load the level through the editor
            loadNewLevelEditor();
        }
    }

    /// <summary>
    /// Save the pillar heights in the layout
    /// </summary>
    private string _savePillarHeights()
    {
        //Holds the layout that will be saved
        string newLayout = "";

        //Get the sides
        GameObject sides = GameObject.Find(transform.name + "/Sides");

        //Go through each side of the arena
        for (int i = 0; i < sides.transform.childCount; i++)
        {
            //Go through each pillar
            for (int j = 0; j < sides.transform.GetChild(i).childCount; j++)
            {
                //Get the PillarSetup
                PillarSetup pillarSetup = sides.transform.GetChild(i).GetChild(j).GetComponent<PillarSetup>();
                //Add the pillar level to the layout
                newLayout += pillarSetup.pillarLevel;

                //Add a comma between pillar levels
                if (j < sides.transform.GetChild(i).childCount - 1)
                {
                    newLayout += ",";
                }

            }

            //Add a colon between sides
            if (i < sides.transform.childCount - 1)
            {
                newLayout += ":";
            }
        }

        return newLayout;
    }

    /// <summary>
    /// Save the item positions, and types
    /// </summary>
    /// <returns></returns>
    private string _saveItemLayout()
    {
        string itemLayout = "";

        int i = 0;

        List<Vector2> keys = new List<Vector2>(itemPlacements.Keys);
        keys.Sort(new Vector2Comparer());

        //Get the keys in the dictionary
        foreach (Vector2 placeMentKey in keys)
        {
            string itemDetails = placeMentKey.x + "," + placeMentKey.y + "|" + itemPlacements[placeMentKey].item.ToString();
            itemLayout += itemDetails;

            if (i < itemPlacements.Count - 1)
            {
                itemLayout += ":";
            }
            i++;

        }


        return itemLayout;
    }

    /// <summary>
    /// Saves the current arena setup
    /// </summary>
    public SaveManager.NewSaveState saveLevel()
    {
        return SaveManager.instance.saveNewLevel(_arenaLayout);
    }

    /// <summary>
    /// Deletes the selected level in the editor
    /// </summary>
    /// <param name="aDeleteReason">Reason that the level was deleted</param>
    public void deleteLevel(string aDeleteReason = null)
    {
        //Delete the selected level
        SaveManager.instance.deleteLevel(currentLevel, aDeleteReason);
    }

    /// <summary>
    /// Reset the arena
    /// </summary>
    public void resetSetup()
    {
        //Set the level to an empty level
        currentLevel = 0;
        _loadSelectedLevel(currentLevel);
        itemPlacements.Clear();
        //Clear items in the level
        BaseObject.destroyAll();

        //Set the arena to a clean state
        setToClean();
    }

    /// <summary>
    /// Load a new level
    /// </summary>
    private void _loadNewLevel()
    {
        if (testGameType != GameState.GameType.SingleLevel)
        {
            //And the newest level hasn't been loaded yet
            if (!loadedNewLevel)
            {
                //If we have reached the end of the set
                if (levelSet.atTheEnd)
                {
                    //Then the player wins
                    GameState.gameManInst.winLevel();
                }
                else
                {
                    //Else keep doing down the list
                    _loadSelectedLevel(levelSet.value);
                    levelSet.levelSelected++;
                }
                loadedNewLevel = true;
            }
        }
    }

    /// <summary>
    /// Create a level set
    /// </summary>
    private void _createLevelSet()
    {
        switch (levelStruct.gameType)
        {
            case GameState.GameType.RandomLevels:
                //Create a level set based on the random number of levels required
                levelSet = new LevelSet<int>();

                //Keep creating levels until we have reached the level cap
                while (levelSet.levels.Count < levelStruct.levelPrefix)
                {
                    //Randomly select a level
                    int levelSelect = (int)(Random.value * (SaveManager.instance.allPossibleLevels.Length - 2) + 1);

                    //If the level has not already been added to the list, add it
                    if (!levelSet.levels.Contains(levelSelect))
                    {
                        levelSet.levels.Add(levelSelect + 1);
                    }
                }
                break;
            case GameState.GameType.LevelSet:
                //If the level is not being tested
                if (!testLevel)
                {
                    //Using the level number load the set
                    string levelSetToLoad = SaveManager.instance.allLevelSets[levelStruct.levelPrefix];
                    string[] levels = levelSetToLoad.Split(',');
                    levelSet = new LevelSet<int>();
                    //Load the levels into the set
                    for (int i = 0; i < levels.Length; i++)
                    {
                        levelSet.levels.Add(int.Parse(levels[i].Trim('\n')));
                    }
                }
                break;
            default:
                //Throw an exception in case we add more modes
                throw new System.NotImplementedException(levelStruct.gameType.ToString() + " is not implemented");
        }
    }

    /// <summary>
    /// Set the editor to dirty, stopping it from making certain changes
    /// </summary>
    public void setToDirty()
    {
        isDirty = true;
    }

    /// <summary>
    /// Set the editor to clean, allowing it to make all changes
    /// </summary>
    public void setToClean()
    {
        isDirty = false;
    }

    #endregion

    void Start()
    {
        //If we are testing random levels
        if (testLevel && testGameType == GameState.GameType.RandomLevels)
        {
            //Create a levele set with 100 levels
            levelStruct = new LevelStructure(GameState.GameType.RandomLevels, 5, 100);
        } 

        _createLevelSet();

        //Load in a new level
        _loadNewLevel();
    }

    void Update()
    {
        //If the player is touching the bottom floor
        if (PlayerBehaviour.instance.currentObjectName.Equals("SouthSide") && !PlayerBehaviour.instance.previousObjectName.Equals(""))
        {
            //Load in a new level
            _loadNewLevel();
        }
        else
        {
            //Then a new level can be loaded when the player touches the bottom floor again
            loadedNewLevel = false;
        }
    }
}
