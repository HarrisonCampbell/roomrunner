﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// Holds level details, and the editor content associated
/// 
/// Editor Layout structure:
/// +--------------+
/// |    Header    |
/// +--------------+
/// |              |
/// |              |
/// |    Content   |
/// |              |
/// |              |
/// +--------------+
/// |    Footer    |
/// +--------------+
/// 
/// </summary>
[System.Serializable]
public class LevelFoldOut
{

    #region Variables

    /// <summary>
    /// Is the content visable
    /// </summary>
    public bool visable;

    /// <summary>
    /// Label on the foldout
    /// </summary>
    public string label;

    /// <summary>
    /// Header content
    /// </summary>
    public EditorContent header;

    /// <summary>
    /// Main content
    /// </summary>
    public EditorContent body;

    /// <summary>
    /// Footer content
    /// </summary>
    public EditorContent footer;

    /// <summary>
    /// Number of lives the player will have
    /// </summary>
    public int lives;

    /// <summary>
    /// Holds essential data depending on the mode
    /// 
    /// RandomLevels
    ///     Number of random levels
    /// LevelSet
    ///     Index of the level set
    /// </summary>
    public int levelPrefix;

    /// <summary>
    /// Scroll View of foldout
    /// </summary>
    public Vector2 scrollView;

    /// <summary>
    /// Game type of the level
    /// </summary>
    public GameState.GameType gameType;

    /// <summary>
    /// Delegate to hold the editor content
    /// </summary>
    public delegate void EditorContent();

    /// <summary>
    /// If the LevelSet mode was selected, store the arenas that were created
    /// </summary>
    public Texture2D[] arenaImages;

    #endregion

    #region  Functions

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="aVisable">Is it folded down</param>
    /// <param name="aLabel">Label on the fold out</param>
    /// <param name="aLives">Amount of lives the player will have</param>
    public LevelFoldOut(bool aVisable, string aLabel, int aLives = 1)
    {
        visable = aVisable;
        label = aLabel;
        lives = aLives;
        scrollView = Vector2.zero;
        gameType = GameState.GameType.RandomLevels;
    }

    #endregion

}


/// <summary>
/// Handles level select controls
/// </summary>
[System.Serializable]
public class WorldSelectTemplate : MonoBehaviour {

    #region Variables

    /// <summary>
    /// A list of all the levels that will appear in the world
    /// </summary>
    [SerializeField,HideInInspector]
    public List<LevelFoldOut> worldLevels;
    
    /// <summary>
    /// The object that is holding the buttons
    /// </summary>
    private GameObject _contentArea;
    
    /// <summary>
    /// The object that is holding the buttons
    /// </summary>
    public GameObject contentArea
    {
        get
        {
            if(_contentArea == null)
            {
                _contentArea = transform.Find("Level View/Scroll View/Viewport/Content").gameObject;
            }
            return _contentArea;
        }
    }

    /// <summary>
    /// Total number of levels in the world
    /// </summary>
    [HideInInspector]
    public int totalLevels = 40;

    #endregion

    #region Functions

    /// <summary>
    /// Clear the levels in the world, and create a new template based on how many levels we want
    /// </summary>
    private void _clearLevels()
    {
        worldLevels = new List<LevelFoldOut>();
        for (int i = 0; i < totalLevels; i++)
        {
            LevelFoldOut levFold = new LevelFoldOut(false, "Level " + (i + 1));
            worldLevels.Add(levFold);
        }
    }

    /// <summary>
    /// Load a level button
    /// </summary>
    /// <param name="aPos">Position of the button</param>
    /// <param name="aLevelNum">The number of the level that will be loaded</param>
    /// <returns>The instance of the button</returns>
    private LevelSelectButton _loadLevelButton(Vector2 aPos,int aLevelNum)
    {
        GameObject lvlButton = Instantiate(Resources.Load("01_Prefabs/LevelButton"),_contentArea.transform) as GameObject;
        lvlButton.transform.localPosition = aPos;
        LevelSelectButton lvlSelect = lvlButton.GetComponent<LevelSelectButton>();
        return lvlSelect;
    }

    /// <summary>
    /// Setup buttons for the level select
    /// </summary>
    public void setUpButtons()
    {
        //Delete all of the buttons placed
        while (contentArea.transform.childCount > 0)
        {
            DestroyImmediate(contentArea.transform.GetChild(0).gameObject);

        }
        //Get the rect transform of the content, and its size
        RectTransform contentRectTrans = contentArea.GetComponent<RectTransform>();
        Vector2 contentSize = contentRectTrans.rect.size;

        //Number of buttons to create
        int buttonsLeft = totalLevels;

        //Set the content y-size to match the number of buttons
        contentRectTrans.sizeDelta = new Vector2(contentRectTrans.sizeDelta.x, (100 * buttonsLeft/4));

        //Create all of the buttons
        while (buttonsLeft > 0)
        {
            int xPos = ((buttonsLeft-1)% ((int)contentSize.x/100)) * 100;
            int yPos = ((buttonsLeft-1) / ((int)contentSize.x / 100)) * -100;
            LevelSelectButton lvlSelectBut = LevelSelectButton.createButton(
                new Vector2(50 + xPos, yPos - 50),
                contentArea.transform,
                buttonsLeft
            );
            buttonsLeft--;
        }

        //Clear the levels too
        _clearLevels();
    }

    #endregion

    void Start()
    {
        //Add the details to the buttons
        for(int i = 0; i < totalLevels; i++)
        {
            GameObject button = contentArea.transform.Find((i + 1).ToString()).gameObject;
            LevelSelectButton lsButton = button.GetComponent<LevelSelectButton>();
            lsButton.levelStruct = new LevelStructure(worldLevels[i].gameType, worldLevels[i].lives, worldLevels[i].levelPrefix);
        }
    }
}
