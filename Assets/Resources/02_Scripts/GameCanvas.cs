﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExtensionClasses.Time;

/// <summary>
/// Canvas for the main game
/// </summary>
public class GameCanvas : MonoBehaviour {

    #region Variables

    /// <summary>
    /// There can only be one game canvas in the scene
    /// </summary>
    private static GameCanvas _instance;

    /// <summary>
    /// There can only be one game canvas in the scene
    /// </summary>
    public static GameCanvas instance
    {
        get
        {
            //If there is no game canvas referenced in the scene
            if(_instance == null)
            {
                //Find the game canvas in the scene
                _instance = FindObjectOfType<GameCanvas>() as GameCanvas;
            }

            return _instance;
        }
    }

    /// <summary>
    /// Handles behaviour of the healthbar
    /// </summary>
    public HealthBarUIController healthBar;

    /// <summary>
    /// The GUI before the game starts
    /// </summary>
    private GameObject _preGameUI;

    /// <summary>
    /// GameObjects that will be displayed when the game is paused
    /// </summary>
    private GameObject _pausedObjects;

    /// <summary>
    /// GameObjects that will be displayed when the game is unpaused
    /// </summary>
    private GameObject _unPausedObjects;

    /// <summary>
    /// Holds the CountDownTimer
    /// </summary>
    public GameObject countDownUI;

    /// <summary>
    /// Which health bar GUI is being interacted with
    /// </summary>
    private int healthBarIndex = 0;

    #endregion

    #region Functions

    /// <summary>
    /// Activate/Deactivate GameState.TimeManagement.Pause
    /// Pauses, or Un-Pauses the game
    /// </summary>
    public void pauseButton()
    {
        //If the game is paused
        if (TimeController.isPaused)
        {
            //Make the player move again
            PlayerBehaviour.instance.startMoving(false);
        }
        //If the game is not paused
        else
        {
            //Pause the game
            TimeController.pause();
        }

        //Flip the active states between pause and unPaused objects
        _pausedObjects.SetActive(!_pausedObjects.activeInHierarchy);
        _unPausedObjects.SetActive(!_unPausedObjects.activeInHierarchy);
    }

    /// <summary>
    /// Display the pause button
    /// </summary>
    /// <param name="aShow">Should the button be displayed</param>
    public void displayPauseButton(bool aShow)
    {
        //Get the pause button
        GameObject pauseButton = _unPausedObjects.transform.Find("PauseButton").gameObject;
        //Display or hide the button
        pauseButton.SetActive(aShow);
    }

    /// <summary>
    /// Start the Game
    /// </summary>
    public void startGameButton(){
        //Hide the pre game UI
        _preGameUI.SetActive(false);

        //Start the game
        GameState.gameManInst.startLevel();
    }

    /// <summary>
    /// Load the Main Menu
    /// </summary>
    public void mainMenuButton()
    {
        Debug.Log("Load Main Menu");
    }

    /// <summary>
    /// Load the Settings
    /// </summary>
    public void settingsButton()
    {
        Debug.Log("Settings Opened");
    }

    /// <summary>
    /// Display the health bar
    /// </summary>
    public void displayHealthBar()
    {
        //If the healthBarIndex does not equal the current health
        while (healthBarIndex != PlayerBehaviour.instance.playerHealth.currentHealth)
        {
            //If the index is greater than the current health of the player
            if (healthBarIndex > PlayerBehaviour.instance.playerHealth.currentHealth)
            {
                //Deplete health
                healthBar.depleteHealthBar(PlayerHealth.maxHealth - healthBarIndex);
                healthBarIndex--;
            }
            //If the index is less than the current health of the player
            else
            {
                //Gain health
                healthBarIndex++;
                healthBar.gainHealthBar(PlayerHealth.maxHealth - healthBarIndex);
            }
        }
    }
    #endregion

    void Start() {
        _preGameUI = transform.Find("Pre-Game UI").gameObject;
        _unPausedObjects = transform.Find("In-Game UI/Un-Paused").gameObject;
        _pausedObjects = transform.Find("In-Game UI/Paused").gameObject;
        
        healthBarIndex = PlayerHealth.maxHealth;
    }

    void Update()
    {
        displayHealthBar();
    }
}

