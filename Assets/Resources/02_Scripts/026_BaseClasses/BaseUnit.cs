﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseUnit : BaseObject {

    #region Variables
    /// <summary>
    /// Units health
    /// </summary>
    private UnitHealth _health;

    /// <summary>
    /// Units health
    /// </summary>
    public UnitHealth health
    {
        get
        {
            if(_health == null)
            {
                _health = gameObject.AddComponent<UnitHealth>();
            }

            return _health;
        }
    }
    #endregion


    #region Functions
    /// <summary>
    /// Unit dies
    /// </summary>
    protected void die()
    {
        destroy();
    }

    public static GameObject create(int x, int y)
    {
        GameObject enemyHolder = GameObject.Find("EnemyHolder");

        if (enemyHolder == null)
        {
            enemyHolder = new GameObject("EnemyHolder");
            enemyHolder.transform.localPosition = new Vector2(-12, 18);
            enemyHolder.transform.localScale = Vector3.one * 3f;
        }
        GameObject newEnemy = create("01_Prefabs/BaseEnemy(Testing)", x, y);
        newEnemy.transform.SetParent(enemyHolder.transform);

        return newEnemy;
    }
    #endregion

    #region Unity_Functions

    void Update()
    {
        //If the player is no longer alive
        if (!health.isAlive)
        {
            //Kill it
            die();
        }
    }
    
    void OnTriggerEnter2D(Collider2D aCollider)
    {
        //If the collider belongs to the player, and they are attacking
        if(aCollider.transform.tag == "Player")
        {
            Debug.LogWarning(aCollider.transform.tag + " attacking?" + PlayerBehaviour.instance.isAttacking);
            if (PlayerBehaviour.instance.isAttacking)
            {
                //Take damage
                health.takeDamage();
            }
        }
    }

    #endregion
}

/// <summary>
/// Standard unit health
/// </summary>
public class UnitHealth : BaseHealth
{
    void Awake()
    {
        maxHealth = 1;
        resetHealth();
    }
}
