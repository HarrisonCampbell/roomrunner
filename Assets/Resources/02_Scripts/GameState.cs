﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ExtensionClasses.Time;

/// <summary>
/// Handles functions that affect the state of the game
/// </summary>
public class GameState : MonoBehaviour
{
    /// <summary>
    /// Handles game management
    /// </summary>
    private static GameManagement _gameManInst;

    /// <summary>
    /// Handles game management
    /// </summary>
    public static GameManagement gameManInst
    {
        get
        {
            //If there is no GameManagement currently created
            if(_gameManInst == null)
            {
                //Find the 'GameState' object and add the GameManagement script to it
                _gameManInst = GameObject.Find("GameState").AddComponent<GameManagement>();
            }
            return _gameManInst;
        }
    }

    /// <summary>
    /// The game type fo the current level
    /// </summary>
    public enum GameType
    {
        RandomLevels,
        LevelSet,
        SingleLevel
    }

    void Awake()
    {
        QualitySettings.vSyncCount = 0;
    }
}

/// <summary>
/// Handles all Game Management
/// </summary>
public class GameManagement : MonoBehaviour
{
    #region Functions

    /// <summary>
    /// Lose the level
    /// </summary>
    public void loseLevel()
    {
        //Reload the level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Start the level
    /// </summary>
    public void startLevel()
    {
        //The player can start moving
        PlayerBehaviour.instance.startMoving(true);
    }

    /// <summary>
    /// Win the level
    /// </summary>
    public void winLevel()
    {
        Debug.Log("The Player wins");
        SceneManager.LoadScene("WinLevel");

    }

    /// <summary>
    /// Quit Game
    /// </summary>
    public static void quitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Load the main game withe the relevate game settings
    /// </summary>
    /// <param name="aLevelStruct">The level structure</param>
    public static void loadLevel(LevelStructure aLevelStruct)
    {
        //Set the player max health
        PlayerHealth.maxHealth = aLevelStruct.lives;

        //Set the level structure
        ArenaSetup.levelStruct = aLevelStruct;

        //Load the main game
        SceneManager.LoadScene("MainGame");
    }

    #endregion
}
