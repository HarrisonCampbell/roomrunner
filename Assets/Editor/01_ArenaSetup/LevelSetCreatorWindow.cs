﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

/// <summary>
/// Helps creates LevelSets
/// </summary>
[Serializable]
public class LevelSetCreatorWindow : EditorWindow
{

    #region Variables

    /// <summary>
    /// Minimum tile size allowed
    /// </summary>
    private int _minTileSize = 10;

    /// <summary>
    /// Maximum tile size allowed
    /// </summary>
    private int _maxTileSize = 20;

    /// <summary>
    /// Current tile size
    /// </summary>
    private int _tileSize = 15;

    /// <summary>
    /// Key that the data will be saved as
    /// </summary>
    private string _saveKey
    {
        get
        {
            return GetType().Name;
        }
    }

    /// <summary>
    /// The level set that we are testing on
    /// </summary>
    private LevelSet<int> testingLevelSet;

    /// <summary>
    /// Total number of levels in the window
    /// </summary>
    private int _localTotalLevels
    {
        set
        {
            EditorPrefs.SetInt(_saveKey + "_NumOfLevels", value);
        }
        get
        {
            return EditorPrefs.GetInt(_saveKey + "_NumOfLevels");
        }
    }
        
    /// <summary>
    /// Vertical scroll position of window
    /// </summary>
    private Vector2 _scrollPos;

    #endregion

    #region Functions

    /// <summary>
    /// Creates an instance of the level set creator
    /// </summary>
    [MenuItem("Custom Tools/Level Creation/Level Set Creator")]
    private static void _init()
    {

        // Get existing open window or if none, make a new one:
        LevelSetCreatorWindow window = (LevelSetCreatorWindow)GetWindow(typeof(LevelSetCreatorWindow));
        window.Show();
    }

    /// <summary>
    /// Draw the level set
    /// </summary>
    /// <param name="aPos">Position to draw the level set</param>
    /// <param name="aTileSize">Tile size</param>
    /// <param name="aRowSize">How many arena's can fit in a row</param>
    private void _drawTheLevelSet(Vector2 aPos, float aTileSize, int aRowSize)
    {
        //Draw each arena
        for (int i = 0; i < testingLevelSet.levels.Count; i++)
        {
            //Get the position of the arena
            Vector2 pos = new Vector2(
                (i % aRowSize) * (ArenaLevelEditor.totalHorizontalPillars + 4) * aTileSize,
                (i / aRowSize) * (ArenaLevelEditor.totalVerticalPillars + 7) * aTileSize
            );

            //Draw the arena
            ArenaDrawingTools.drawArena(testingLevelSet.levels[i], pos + aPos, Vector2.one * aTileSize);

            //If the user clicks one the arena's, select it
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                Vector2 mousePos = Event.current.mousePosition;

                if (
                    mousePos.x > (pos.x + aPos.x) && 
                    mousePos.y > (pos.y + aPos.y) && 
                    mousePos.x < (pos.x + aPos.x) + (ArenaLevelEditor.totalHorizontalPillars + 2) * aTileSize && 
                    mousePos.y < (pos.y + aPos.y) + (ArenaLevelEditor.totalVerticalPillars + 2) * aTileSize)
                {
                    testingLevelSet.levelSelected = i;
                    Repaint();
                }

            }

            //If the arena is selected highlight it
            if (i == testingLevelSet.levelSelected)
            {
                ArenaDrawingTools.drawRect(aPos + pos, new Vector2(ArenaLevelEditor.totalHorizontalPillars + 2, ArenaLevelEditor.totalVerticalPillars + 2) * aTileSize, new Color(0, 1, 1, 0.5f));
            }

            //Draws lines to the next arena
            if (i < testingLevelSet.levels.Count - 1)
            {
                //Draws from the last arena in the previous row to the first arena in the next row
                if ((i + 1) % aRowSize == 0)
                {
                    ArenaDrawingTools.drawRect(
                        aPos + pos + new Vector2(((ArenaLevelEditor.totalHorizontalPillars + 1) / 2) * aTileSize, (ArenaLevelEditor.totalVerticalPillars + 2) * aTileSize),
                        new Vector2(aTileSize, aTileSize * 2),
                        Color.white);
                    ArenaDrawingTools.drawRect(
                        aPos + new Vector2((ArenaLevelEditor.totalVerticalPillars / 2 - 1) * aTileSize, pos.y + (ArenaLevelEditor.totalVerticalPillars + 4) * aTileSize),
                        new Vector2((ArenaLevelEditor.totalVerticalPillars * (aRowSize - 1)) + 1, 1) * aTileSize,
                        Color.white
                        );
                    ArenaDrawingTools.drawRect(
                        aPos + new Vector2((ArenaLevelEditor.totalVerticalPillars / 2 - 1) * aTileSize, pos.y + (ArenaLevelEditor.totalVerticalPillars + 4) * aTileSize),
                        new Vector2(1, 3) * aTileSize,
                        Color.white
                        );
                }
                else
                {
                    //Draw a line to the arena to the right
                    ArenaDrawingTools.drawRect(
                        aPos + pos + new Vector2((ArenaLevelEditor.totalHorizontalPillars + 2) * aTileSize, (ArenaLevelEditor.totalVerticalPillars / 2 + 1) * aTileSize),
                        new Vector2(aTileSize * 2, aTileSize),
                        Color.white);
                }
            }
        }
    }

    /// <summary>
    /// What buttons can be used for the LevelSetCreator
    /// </summary>
    private void _levelSetController()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Level"))
        {
            testingLevelSet.levels.Add(ArenaSetup.instance.currentLevel);
        }

        if (GUILayout.Button("Remove Level"))
        {
            testingLevelSet.remove();
        }

        if (GUILayout.Button("Save Layout"))
        {
            //Save the level set
            SaveManager.NewSaveState saveState = SaveManager.instance.saveNewLevelSet(testingLevelSet.ToString());

            //If the save was unsuccesful
            if (!saveState.saveSuccesful)
            {
                //If its a duplicate
                if(saveState.type == SaveManager.NewSaveState.UnsuccesfulType.DUPLICATE)
                {
                    //Create a notice stating that the save was a duplicate
                    NoticePopUp.create(
                        new Vector2(600, 300),
                        "Level Set Duplicate",
                        "The Level Set you are trying to save, is already in the database"
                        );
                }
            }

            AssetDatabase.Refresh();
        }

        if(GUILayout.Button("Clear Layout"))
        {
            testingLevelSet.levels.Clear();
        }

        if (GUILayout.Button("← ShiftDown"))
        {
            testingLevelSet.shiftDown();
        }

        if (GUILayout.Button("→ Shift Up"))
        {
            testingLevelSet.shiftUp();
        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        _tileSize = (int)GUILayout.HorizontalSlider(_tileSize, _minTileSize, _maxTileSize);
        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// Load the data locally
    /// </summary>
    private void _loadDataLocally()
    {
        int numOfLevels = _localTotalLevels;
        testingLevelSet = new LevelSet<int>();
        for (int i = 0; i < numOfLevels; i++)
        {
            int level = EditorPrefs.GetInt(_saveKey + "_Data_" + i);
            testingLevelSet.levels.Add(level);
        }
    }

    /// <summary>
    /// Save the data locally
    /// </summary>
    private void _saveDataLocally()
    {
        //When the window is disabled save the local data
        _localTotalLevels = testingLevelSet.levels.Count;
        for (int i = 0; i < _localTotalLevels; i++)
        {
            EditorPrefs.SetInt(_saveKey + "_Data_" + i, testingLevelSet.levels[i]);
        }
    }

    /// <summary>
    /// Display the contents of the window
    /// </summary>
    private void _displayWindowContents()
    {
        //Controller for the LevelSetCreator Window
        _levelSetController();

        //Scroll position of the window
        _scrollPos = GUILayout.BeginScrollView(_scrollPos);

        //Size of the arena
        Vector2 sizeOfArena = new Vector2((ArenaLevelEditor.totalHorizontalPillars + 4), (ArenaLevelEditor.totalVerticalPillars + 7)) * _tileSize;
        int xNumArena = (int)(position.width / sizeOfArena.x);
        int yNumArena = (testingLevelSet.levels.Count / xNumArena);
        if (testingLevelSet.levels.Count % xNumArena != 0)
        {
            yNumArena++;
        }

        // Area that is needed to view all of the arenas
        GUILayout.Space(yNumArena * sizeOfArena.y);

        //Draw the LevelSet
        _drawTheLevelSet(new Vector2(5, 25), _tileSize, xNumArena);

        GUILayout.EndScrollView();
    }

    #endregion

    #region Unity Functions

    void OnGUI()
    {
        //If there is an arena in the scene
        if (ArenaSetup.inScene)
        {
            //Display the LevelSet
            _displayWindowContents();
        }
        else
        {
            GUILayout.Label("There is no Arena in the scene");
        }
        
    }

    void OnEnable()
    {
        //Load the data
        _loadDataLocally();

        //If we are testing the level set, load in the data from the LevelSetCreatorWindow
        if (ArenaSetup.instance.testLevel && ArenaSetup.instance.testGameType == GameState.GameType.LevelSet)
        {
            LevelStructure testLevelStruct = new LevelStructure(GameState.GameType.LevelSet, 5, 100);
            ArenaSetup.levelSet = testingLevelSet;
            ArenaSetup.levelStruct = testLevelStruct;
        }
    }

    void OnDisable()
    {
        //Save the data
         _saveDataLocally();
    }

    #endregion

}
