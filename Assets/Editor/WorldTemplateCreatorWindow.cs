﻿using UnityEngine;
using UnityEditor;
using System;
using ExtensionClasses.CSharp.Array;

/// <summary>
/// Window that holds the information of the game world
/// </summary>
[System.Serializable]
public class WorldTemplateCreatorWindow : EditorWindow
{

    #region Variables

    /// <summary>
    /// The instance of the WorldSelectTemplate
    /// </summary>
    [SerializeField]
    private WorldSelectTemplate _worldSelectTemplate;

    /// <summary>
    /// Scroll position of the window
    /// </summary>
    private Vector2 _scrollPosition;

    #endregion

    #region Functions

    /// <summary>
    /// Set the default header variables
    /// </summary>
    /// <param name="aFoldOut">FoldOut to add the info to</param>
    private void _setDefaultHeader(LevelFoldOut aFoldOut)
    {
        aFoldOut.header += () => EditorGUILayout.BeginHorizontal();
        aFoldOut.header += () => aFoldOut.gameType = (GameState.GameType)EditorGUILayout.EnumPopup("Game Mode", aFoldOut.gameType, GUILayout.Width(300));
        aFoldOut.header += () => aFoldOut.lives = EditorGUILayout.IntSlider("Lives : " + aFoldOut.lives, aFoldOut.lives, 1, 5);
        aFoldOut.header += () => GUILayout.FlexibleSpace();
        aFoldOut.header += () => EditorGUILayout.EndHorizontal();
    }

    /// <summary>
    /// Initialise the WorldTemplateCreator window
    /// </summary>
    /// <param name="aLevels">Number of levels the world will have</param>
    /// <return>The window</return>
    public static WorldTemplateCreatorWindow init(WorldSelectTemplate aWorldSelect)
    {
        // Get existing open window or if none, make a new one:
        WorldTemplateCreatorWindow window = (WorldTemplateCreatorWindow)GetWindow(typeof(WorldTemplateCreatorWindow));

        //Set up the default details
        window._worldSelectTemplate = aWorldSelect;

        //Display the default constructor, if the content is null
        if (window._worldSelectTemplate.worldLevels != null)
        {
            for (int i = 0; i < window._worldSelectTemplate.worldLevels.Count; i++)
            {
                if (window._worldSelectTemplate.worldLevels[i].header == null)
                {
                    window._setDefaultHeader(window._worldSelectTemplate.worldLevels[i]);
                }
            }
        }

        window.Show();
        return window;
    }

    /// <summary>
    /// Display the content of the foldout
    /// </summary>
    /// <param name="aFoldOut">Editor content to display</param>
    private void _displayFoldout(LevelFoldOut aFoldOut)
    {
        aFoldOut.visable = EditorGUILayout.Foldout(aFoldOut.visable, aFoldOut.label);
        //If the foldout is displayed
        if (aFoldOut.visable)
        {
            //And there is content to show, show it
            if (aFoldOut.header != null)
            {
                aFoldOut.header();
            }
            if (aFoldOut.body != null)
            {
                aFoldOut.body();
            }
            if (aFoldOut.footer != null)
            {
                aFoldOut.footer();
            }
        }
    }

    /// <summary>
    /// The buttons used to control the world template
    /// </summary>
    private void _worldTemplateController()
    {
        GUILayout.BeginHorizontal();

        ///Generates a list of levels
        if (GUILayout.Button("Generate Level Structure"))
        {
            _worldSelectTemplate.setUpButtons();

            for (int i = 0; i < _worldSelectTemplate.worldLevels.Count; i++)
            {
                _setDefaultHeader(_worldSelectTemplate.worldLevels[i]);
            }

        }

        //Total number of levels we want
        _worldSelectTemplate.totalLevels = EditorGUILayout.IntField(_worldSelectTemplate.totalLevels);

        GUILayout.EndHorizontal();

        //Add a new level to the end of the list
        if (GUILayout.Button("Add New Level"))
        {
            throw new NotImplementedException("Add new level not implemented");
        }

        //Remove a leve at the end of the list
        if (GUILayout.Button("Remove End of Level"))
        {
            throw new NotImplementedException("Add Remove end of level not implemented");
        }

    }

    /// <summary>
    /// Get the grid of the arena
    /// </summary>
    /// <param name="aArenaIndex">Arena index</param>
    /// <returns>A 2D array representation of the arena</returns>
    private int[,] _arenaGrid(int aArenaIndex)
    {
        //Create a double dimension array to hold the arena items
        int[,] grid = new int[ArenaLevelEditor.totalVerticalPillars + 2, ArenaLevelEditor.totalHorizontalPillars + 2];

        //Get the layout of the index given
        string layout = SaveManager.instance.allPossibleLevels[aArenaIndex];

        //Get the pilar levels, and the item layout
        string pillarLayouts = layout.Split('-')[0];
        string itemLayout = layout.Split('-')[1];

        //Get the pillar pos
        //For through each side and draw it out
        string[] arenaSides = pillarLayouts.Split(':');
        string[] eastSide = arenaSides[0].Split(',');
        string[] northSide = arenaSides[1].Split(',');
        string[] westSide = arenaSides[2].Split(',');
        string[] southSide = arenaSides[3].Split(',');

        //EastSide
        for (int i = 0; i < eastSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(eastSide[i]) + 1; j++)
            {
                grid[(i * -1) + ArenaLevelEditor.totalVerticalPillars, ArenaLevelEditor.totalHorizontalPillars - j + 1] = 1;
            }
        }

        //NorthSide
        for (int i = 0; i < northSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(northSide[i]) + 1; j++)
            {
                grid[j, ArenaLevelEditor.totalHorizontalPillars - i] = 1;
            }
        }

        //WestSide
        for (int i = 0; i < westSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(westSide[i]) + 1; j++)
            {
                grid[i + 1, j] = 1;
            }
        }

        //SouthSide
        for (int i = 0; i < southSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(southSide[i]) + 1; j++)
            {
                grid[ArenaLevelEditor.totalVerticalPillars + 1 - j, i + 1] = 1;
            }
        }

        //If there are items in the arena
        if (!string.IsNullOrEmpty(itemLayout))
        {
            //Draw the items
            Debug.Log("Items : " + itemLayout);
            foreach (string item in itemLayout.Split(':'))
            {
                string xPos = item.Split('|')[0].Split(',')[0].ToString(), yPos = item.Split('|')[0].Split(',')[1].ToString();
                Vector2 location = new Vector2(int.Parse(yPos), int.Parse(xPos));
                ArenaSetup.Item itemType = (ArenaSetup.Item)Enum.Parse(typeof(ArenaSetup.Item), item.Split('|')[1]);

                switch (itemType)
                {
                    case ArenaSetup.Item.ENEMY:
                        grid[(int)location.x + 1, (int)location.y+1] = 2;
                        break;
                    default:
                        throw new NotImplementedException("Item type not supported");
                }
            }
        }

        //Rotate the grid
        for (int i = 0; i < 3; i++)
        {
            grid.rotateCounterClockwise();
        }

        return grid;
    }

    /// <summary>
    /// Draw the texture of the arena
    /// </summary>
    /// <param name="aArenaIndex">The arena we want to draw</param>
    /// <param name="aScale">Scale of the texture</param>
    /// <returns></returns>
    private Texture2D _drawArena(int aArenaIndex, int aScale)
    {
        //Grid of the arena
        int[,] arenaGrid = _arenaGrid(aArenaIndex);

        GUIStyle boxStyle = GUI.skin.box;
        RectOffset boxPadding = boxStyle.padding;

        Texture2D arenaTex = ArenaDrawingTools.makeArenaTexture(arenaGrid, aScale);

        boxStyle.padding = new RectOffset(2, 2, 2, 2);
        GUILayout.Box(
            arenaTex,
            boxStyle,
            GUILayout.Width(arenaGrid.GetLength(0) * aScale),
            GUILayout.Height(arenaGrid.GetLength(1) * aScale)
            );
        boxStyle.padding = boxPadding;

        return arenaTex;
    }

    /// <summary>
    /// View if the level is a RandomLevel
    /// </summary>
    /// <param name="aFoldOut"></param>
    private void _randomLevelView(LevelFoldOut aFoldOut)
    {
        aFoldOut.levelPrefix = EditorGUILayout.IntSlider("Total Levels : " + aFoldOut.levelPrefix, aFoldOut.levelPrefix, 1, 100);
    }

    /// <summary>
    /// View if the level is a LevelSet
    /// </summary>
    /// <param name="aFoldOut"></param>
    private void _levelSetView(LevelFoldOut aFoldOut)
    {
        int oldLevel = aFoldOut.levelPrefix;
        aFoldOut.levelPrefix = EditorGUILayout.IntSlider("Level Set index : " + aFoldOut.levelPrefix, aFoldOut.levelPrefix, 0, SaveManager.instance.allLevelSets.Length - 2);

        aFoldOut.scrollView = GUILayout.BeginScrollView(aFoldOut.scrollView, GUILayout.ExpandHeight(false));

        //Get the arenas from the index
        String[] levels = SaveManager.instance.allLevelSets[aFoldOut.levelPrefix].Split(',');
        int scale = 10;
        GUILayout.BeginHorizontal();
        if (oldLevel != aFoldOut.levelPrefix)
        {
            aFoldOut.arenaImages = new Texture2D[levels.Length];
            for (int i = 0; i < levels.Length; i++)
            {
                aFoldOut.arenaImages[i] = _drawArena(int.Parse(levels[i].Trim('\n')), scale);
            }
        }
        else
        {
            if (aFoldOut.arenaImages != null)
            {
                foreach(Texture2D arenaTexture in aFoldOut.arenaImages) {
                        GUIStyle boxStyle = GUI.skin.box;
                        RectOffset boxPadding = boxStyle.padding;

                        boxStyle.padding = new RectOffset(2, 2, 2, 2);
                        GUILayout.Box(
                            arenaTexture,
                            boxStyle
                            );
                        boxStyle.padding = boxPadding;
                    }
                
            }

        }


        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();

    }

    /// <summary>
    /// Displays all the stuff in the window
    /// </summary>
    private void _displayWindowContents()
    {
        //_scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

        _worldTemplateController();

        if (_worldSelectTemplate.worldLevels != null)
        {
            //Go through each foldout and display the content
            foreach (LevelFoldOut foldOut in _worldSelectTemplate.worldLevels)
            {
                if (foldOut.gameType == GameState.GameType.LevelSet)
                {
                    foldOut.body = () => _levelSetView(foldOut);
                }
                else if (foldOut.gameType == GameState.GameType.RandomLevels)
                {
                    foldOut.body = () => _randomLevelView(foldOut);
                }
                else
                {
                    throw new NotImplementedException("Game mode not supported");
                }

                _displayFoldout(foldOut);
            }
        }

        //EditorGUILayout.EndScrollView();
    }

    #endregion

    #region Unity Functions

    void OnGUI()
    {
        if (_worldSelectTemplate != null)
        {
            _displayWindowContents();
        }
        else
        {
            Close();
        }
    }

    #endregion

}
