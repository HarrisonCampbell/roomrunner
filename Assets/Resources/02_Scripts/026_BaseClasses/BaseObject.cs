﻿using UnityEngine;

/// <summary>
/// Base class for all interactable objects in game
/// </summary>
public abstract class BaseObject: MonoBehaviour {

    #region Functions

    /// <summary>
    /// Create object
    /// </summary>
    /// <param name="aPath">Path the object being created</param>
    /// <param name="aX">Coloumn</param>
    /// <param name="aY">Row</param>
    public static GameObject create(string aPath,int aX, int aY)
    {

        GameObject obj = Instantiate(Resources.Load(aPath)) as GameObject;

        //Get the arena sides
        GameObject sides = GameObject.Find(ArenaSetup.instance.name + "/Sides");
        GameObject eastSide = sides.transform.GetChild(0).gameObject;
        GameObject northSide = sides.transform.GetChild(1).gameObject;

        //Set the position of the object
        obj.transform.position = new Vector3((aX - northSide.transform.childCount/2)*3, (aY - eastSide.transform.childCount / 2) * -3);

        return obj;
    }

    /// <summary>
    /// Destroys the object
    /// </summary>
    public void destroy()
    {
        //If the game is running
        if (Application.isPlaying)
        {
            Destroy(gameObject);
        //If in the editor mode
        }else if (Application.isEditor)
        {
            DestroyImmediate(gameObject);
        }
    }

    /// <summary>
    /// Destroy all of the objects in the scene
    /// </summary>
    public static void destroyAll()
    {
        BaseObject[] allBaseObjects = FindObjectsOfType<BaseObject>() as BaseObject[];
        for(int i = 0; i < allBaseObjects.Length; i++)
        {
            allBaseObjects[i].destroy();
        }
    }

    #endregion
}
