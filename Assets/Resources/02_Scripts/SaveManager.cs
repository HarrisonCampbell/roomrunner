﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// Handles all saving and loading in the game
/// </summary>
public class SaveManager : MonoBehaviour {

    #region Struct

    /// <summary>
    /// Holds the save states
    /// </summary>
    public struct NewSaveState
    {
        /// <summary>
        /// How the save state was unsuccesful 
        /// </summary>
        public enum UnsuccesfulType
        {
            NONE,
            DUPLICATE,
            DELETED
        }

        /// <summary>
        /// Was the save succesful
        /// </summary>
        public bool saveSuccesful;

        /// <summary>
        /// If the save was unsuccesful, then how?
        /// </summary>
        public UnsuccesfulType type;

        /// <summary>
        /// The level save data
        /// </summary>
        public string levelSaveData;

        /// <summary>
        /// Create a new level save state
        /// </summary>
        /// <param name="aType">UnsuccesfulType</param>
        /// <param name="aLevelSaveData">The save data</param>
        /// <returns>New level save state</returns>
        public static NewSaveState newSaveState(UnsuccesfulType aType, string aLevelSaveData = null)
        {
            NewSaveState newLevelSave = new NewSaveState();
            newLevelSave.type = aType;
            newLevelSave.levelSaveData = aLevelSaveData;
            if (aType == UnsuccesfulType.NONE)
            {
                newLevelSave.saveSuccesful = true;
            }
            else
            {
                newLevelSave.saveSuccesful = false;
            }


            return newLevelSave;
        }

        /// <summary>
        /// Succesful save
        /// </summary>
        /// <returns>A succesful save</returns>
        public static NewSaveState succesfulSave()
        {
            return newSaveState(UnsuccesfulType.NONE);
        }
    }

    #endregion

    #region Variables

    /// <summary>
    /// Path to text folder
    /// </summary>
    private string _dataPathToTextFolder
    {
        get
        {
            return Application.dataPath + "/Resources/07_Text/";
        }
    }

    /// <summary>
    /// Get the path to the Layouts text
    /// </summary>
    public string dataPathToLayoutText
    {
        get
        {
            return _dataPathToTextFolder + "Layouts.txt";
        }
    }

    /// <summary>
    /// Get the path to the LevelSet text
    /// </summary>
    public string dataPathToLevelSetText
    {
        get
        {
            return _dataPathToTextFolder + "LevelSet.txt";
        }
    }

    /// <summary>
    /// Get the path to the Deleted Levels text
    /// </summary>
    public string dataPathToDeletedLevelsText
    {
        get
        {
            return _dataPathToTextFolder + "DeletedLevels.txt";
        }
    }

    /// <summary>
    /// There can only be one save manager
    /// </summary>
    private static SaveManager _instance;

    /// <summary>
    /// There can only be one save manager
    /// </summary>
    public static SaveManager instance
    {
        get
        {
            //If the save manager is not referenced
            if(_instance == null)
            {

                //Try to find the save manager in the scene
                GameObject saveManagerObject = GameObject.Find("SaveManager");
                if(GameObject.Find("SaveManager") == null)
                {
                    //If there is no save manager then create a new one
                    saveManagerObject = new GameObject("SaveManager");
                    _instance = saveManagerObject.AddComponent<SaveManager>();

                    //Get the text to load
                    _instance.layoutsText = Resources.Load("07_Text/Layouts") as TextAsset;
                    _instance.levelSetText = Resources.Load("07_Text/LevelSet") as TextAsset;
                }
                else
                {
                    _instance = saveManagerObject.GetComponent<SaveManager>();
                }
            }

            return _instance;
        }
    }

    /// <summary>
    /// Text file that contains all the possible layouts
    /// </summary>
    public TextAsset layoutsText;

    /// <summary>
    /// Holds all possible levels
    /// </summary>
    public string[] allPossibleLevels
    {
        get
        {
            return layoutsText.text.Split(new char[] { '\r' });
        }
    }

    /// <summary>
    /// Text file that contains all the possible level sets
    /// </summary>
    public TextAsset levelSetText;

    /// <summary>
    /// Holds all of the level sets
    /// </summary>
    public string[] allLevelSets
    {
        get
        {
            return levelSetText.text.Split(new char[] { '\r' });
        }
    }

    #endregion

    #region Functions

    /// <summary>
    /// Checks if the new level saved is a unique layout
    /// </summary>
    /// <param name="aNewLevelPath">New layout that might be saved</param>
    /// <returns>Is the level saved unique</returns>
    private bool _uniqueLevelCreated(string aNewLevelPath, string[] aLevels)
    {

        //Go through each layout
        for (int i = 0; i < aLevels.Length; i++)
        {
            //Get the layout
            string prevLayout = aLevels[i].Split('\r')[0];

            //If the old layout is the same as the new layout
            if (aNewLevelPath.Equals(prevLayout.TrimStart('\n')))
            {
                return false;
            }
        }

        // If no exceptions have been thrown then the level must be unique
        return true;
    }

    /// <summary>
    /// Checks if the new level saved is unique
    /// </summary>
    /// <param name="aNewLevelPath"></param>
    /// <param name="aLevel"></param>
    /// <returns></returns>
    private bool _uniqueLevelCreated(string aNewLevelPath, string aLevel)
    {
        return _uniqueLevelCreated(aNewLevelPath, new string[] { aLevel });
    }

    /// <summary>
    /// Handles saving a new level
    /// </summary>
    /// <param name="aArenaLayout"></param>
    /// <returns></returns>
    public NewSaveState saveNewLevel(string aArenaLayout)
    {
        //Open a stream at the Layouts folder
        StreamWriter saveFile = File.AppendText(dataPathToLayoutText);

        string[] allDeletedLevels = File.ReadAllLines(dataPathToDeletedLevelsText);

        List<string> dLevels = new List<string>();
        for (int i = 0; i < allDeletedLevels.Length; i++)
        {
            if (!string.IsNullOrEmpty(allDeletedLevels[i]))
            {
                dLevels.Add(allDeletedLevels[i].Split('@')[1]);
            }
        }

        for (int i = 0; i < dLevels.Count; i++)
        {
            //Check that the file that is being saved is not already been deleted
            if (!_uniqueLevelCreated(aArenaLayout, dLevels[i]))
            {
                string[] levelDesc = allDeletedLevels[i].Split('@')[0].Split('-');
                saveFile.Close();
                return NewSaveState.newSaveState(
                    NewSaveState.UnsuccesfulType.DELETED,
                    string.Format("On {0}, {1}", levelDesc[1], levelDesc[0])
                    );
            }
        }

        //If the layout saved unique
        if (_uniqueLevelCreated(aArenaLayout, allPossibleLevels))
        {
            //Write it to the file
            saveFile.WriteLine(aArenaLayout);
            //Close the save file
            saveFile.Close();
        }
        else
        {
            //Close the save file
            saveFile.Close();

            return NewSaveState.newSaveState(SaveManager.NewSaveState.UnsuccesfulType.DUPLICATE);
        }

        //Return a succesful save
        return SaveManager.NewSaveState.succesfulSave();
    }

    /// <summary>
    /// Delete a level in the database
    /// </summary>
    /// <param name="aLevelIndex">The level that we want to delete</param>
    /// <param name="aDeleteReason">The reason that the level is being deleted</param>
    public void deleteLevel(int aLevelIndex, string aDeleteReason = null)
    {

        //Today's date
        string currentDate = System.DateTime.Now.ToLongDateString();

        //Get the currentLevel string
        string currentLevel = allPossibleLevels[aLevelIndex];

        //Convert levels to a list
        List<string> allLevels = new List<string>(allPossibleLevels);

        //Remove the level that was deleted
        allLevels.Remove(currentLevel);

        File.Delete(instance.dataPathToLayoutText);
        StreamWriter saveFile = File.AppendText(dataPathToLayoutText);
        for (int i = 0; i < allLevels.Count; i++)
        {
            string newLine = allLevels[i].TrimStart('\n');
            if (!string.IsNullOrEmpty(newLine))
            {
                saveFile.WriteLine(newLine);
            }
        }
        saveFile.Close();

        //Save the deleted level, and the reason to another file
        StreamWriter deletedLevelFile = File.AppendText(dataPathToDeletedLevelsText);

        deletedLevelFile.WriteLine(aDeleteReason + "-" + currentDate + "@" + currentLevel.TrimStart('\n'));

        deletedLevelFile.Close();
    }

    /// <summary>
    /// Checks if the level is in a set
    /// </summary>
    /// <param name="aLevelIndex">Level that we are checking</param>
    /// <returns>True if the level is in a set</returns>
    public bool isLevelInASet(int aLevelIndex)
    {
        //Check that the level being deleted is not in a level set
        for (int i = 0; i < allLevelSets.Length; i++)
        {
            string levels = allLevelSets[i].Split('-')[0];

            foreach (string level in levels.Split(',', ':'))
            {
                //if the level is the same as the index, then its in a set
                if (int.Parse(level) == aLevelIndex)
                {
                    return true;
                }
            }
        }

        //If we reached here than the level is not a part of the set
        return false;
    }

    /// <summary>
    /// Handles saving a new level set
    /// </summary>
    /// <param name="aLevelSet">Level Set that we want to save</param>
    /// <returns>Returns weather the save was succesful or not</returns>
    public NewSaveState saveNewLevelSet(string aLevelSet)
    {
        //If the levelset was already saved
        if (!_uniqueLevelCreated(aLevelSet, allLevelSets))
        {
            //Then return a duplicate save state
            return NewSaveState.newSaveState(NewSaveState.UnsuccesfulType.DUPLICATE);
        }


        //If we reached here than the save was succesful
        StreamWriter saveFile = File.AppendText(dataPathToLevelSetText);
        saveFile.WriteLine(aLevelSet);
        saveFile.Close();
        return NewSaveState.succesfulSave();
    }

    #endregion

}
