﻿using UnityEngine;
using UnityEngine.UI;
using ExtensionClasses.Math;
using ExtensionClasses.Time;
using ExtensionClasses.Unity.LayerMask;
using System.Collections;

/// <summary>
/// How the Player interacts with the environment
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(PlayerHealth))]
public class PlayerBehaviour : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// The normal vector of the side that the player just detached from 
    /// </summary>
    private Vector2 _prevSideNormalVector;

    /// <summary>
    /// The normal vector of the side the player is currently on
    /// </summary>
    private Vector2 _currentSideNormalVector
    {
        get
        {
            if (currentObjectName.Equals("NorthSide"))
            {
                return Vector2.up;
            }
            else if (currentObjectName.Equals("EastSide"))
            {
                return Vector2.right;
            }
            else if (currentObjectName.Equals("SouthSide"))
            {
                return Vector2.down;
            }
            else if (currentObjectName.Equals("WestSide"))
            {
                return Vector2.left;
            }
            else
            {
                throw new System.NotSupportedException("Current side selected is not valid");
            }
        }
    }

    /// <summary>
    /// There can only be one one character in the scene
    /// </summary>
    public static PlayerBehaviour instance;

    /// <summary>
    /// Power of the players jump
    /// </summary>
    public float jumpPower = 4f;

    /// <summary>
    /// How fast the player is moving
    /// </summary>
    public float moveSpeed = 4f;

    /// <summary>
    /// The Players Rigidbody2d
    /// </summary>
    private Rigidbody2D _rig2d;

    /// <summary>
    /// The Players Rigidbody2D
    /// </summary>
    public Rigidbody2D rig2d
    {
        get
        {
            //If there is no Rigidbody referenced
            if (_rig2d == null)
            {
                //Find the Ridigbody from the Gameobject
                _rig2d = GetComponent<Rigidbody2D>();
            }

            return _rig2d;
        }
    }

    /// <summary>
    /// Health of the player
    /// </summary>
    public PlayerHealth playerHealth;

    /// <summary>
    /// Is the player currently grounded
    /// </summary>
    private bool _isGrounded;

    /// <summary>
    /// Can the player move
    /// </summary>
    private bool _canMove = false;

    /// <summary>
    /// Name of the side the player was previously on
    /// </summary>
    public string previousObjectName = "";

    /// <summary>
    /// Name of the side the player is currently on
    /// </summary>
    public string currentObjectName = "";

    /// <summary>
    /// Is the player attacking
    /// </summary>
    private bool _isAttacking = false;

    /// <summary>
    /// Is the player attacking
    /// </summary>
    public bool isAttacking
    {
        get
        {
            return _isAttacking;
        }
    }

    /// <summary>
    /// Gravity scale of the player
    /// </summary>
    private float _playerGravScale;

    /// <summary>
    /// The attack that the player is using
    /// </summary>
    private Coroutine _attack;

    /// <summary>
    /// Starting position of the player, when the game begins
    /// </summary>
    private Vector3 _startingPos;

    #endregion

    #region Functions

    /// <summary>
    /// Rotate the gravity
    /// </summary>
    /// <param name="aCollObj">Object the player collided with</param>
    private void _checkForGravityRotation(Collision2D aCollObj)
    {
        //If the current side has been set
        if (!string.IsNullOrEmpty(currentObjectName))
        {
            //If the player is colliding with a new side
            if (!aCollObj.transform.parent.name.Equals(currentObjectName))
            {
                //If the previous side has been set
                if (!string.IsNullOrEmpty(previousObjectName))
                {
                    //If the player has not collided with the previous side
                    if (!aCollObj.transform.parent.name.Equals(previousObjectName))
                    {
                        //Rotate the Gravity of the player
                        _rotateGravity(aCollObj.transform.parent.name);
                    }

                }
                //Else if it hasn't been set
                else
                {
                    //Rotate the Gravity of the player
                    _rotateGravity(aCollObj.transform.parent.name);
                }
            }
        }
        //Else if the first side isn't set yet
        else
        {
            //Get the side name
            currentObjectName = aCollObj.transform.parent.name;
        }
    }

    /// <summary>
    /// Rotate the Gravity
    /// </summary>
    /// <param name="aNewSurface">If the new surface is being collided with</param>
    /// <param name="aResetToNormal">Should we reset the gravity back to normal</param>
    private void _rotateGravity(string aNewSurface = "", bool aResetToNormal = false)
    {
        //If the gravity is not being set to the starting gravity
        if (!aResetToNormal)
        {
            //Rotate the gravity so that the player can move along the new surface and, set the previous side to the current side and the current side to the newSurface
            _prevSideNormalVector = _currentSideNormalVector;
            previousObjectName = currentObjectName;
            currentObjectName = aNewSurface;
            Physics2D.gravity = VectorRotation.rotateVector2D(Physics2D.gravity, Vector2.Angle(_prevSideNormalVector, _currentSideNormalVector));
        }
        //Else If the gravity is being set to the starting gravity
        else
        {
            //Just rotate the gravity by 90 degrees
            Physics2D.gravity = VectorRotation.rotateVector2D(Physics2D.gravity, 90f);
        }
    }

    /// <summary>
    /// Make the character jump
    /// </summary>
    public void jump()
    {
        //If the player is grounded, and can move
        if (_isGrounded && _canMove)
        {
            //Make them jump, relative to the gravity of the world
            if (Physics2D.gravity.normalized == Vector2.down)
            {
                rig2d.velocity = new Vector2(rig2d.velocity.x, jumpPower);
            }
            else if (Physics2D.gravity.normalized == Vector2.right)
            {
                rig2d.velocity = new Vector2(-jumpPower, rig2d.velocity.y);
            }
            else if (Physics2D.gravity.normalized == Vector2.up)
            {
                rig2d.velocity = new Vector2(rig2d.velocity.x, -jumpPower);
            }
            else if (Physics2D.gravity.normalized == Vector2.left)
            {
                rig2d.velocity = new Vector2(jumpPower, rig2d.velocity.y);

            }
        }

    }

    /// <summary>
    /// Move the player forward depending on the gravity
    /// </summary>
    private void _move()
    {
        //If the player can move
        if (_canMove)
        {
            //Move the player along the surface they have last touched
            if (Physics2D.gravity.normalized == Vector2.down)
            {
                rig2d.velocity = new Vector2(moveSpeed, rig2d.velocity.y);
            }
            else if (Physics2D.gravity.normalized == Vector2.right)
            {
                rig2d.velocity = new Vector2(rig2d.velocity.x, moveSpeed);
            }
            else if (Physics2D.gravity.normalized == Vector2.up)
            {
                rig2d.velocity = new Vector2(-moveSpeed, rig2d.velocity.y);
            }
            else if (Physics2D.gravity.normalized == Vector2.left)
            {
                rig2d.velocity = new Vector2(rig2d.velocity.x, -moveSpeed);

            }
        }
    }

    /// <summary>
    /// Start moving the player
    /// </summary>
    /// <param name="aStartOfGame">Is it the start of the game?</param>
    public void startMoving(bool aStartOfGame)
    {
        //Start the countdown timer
        StartCoroutine(IStartCountDownTimer(aStartOfGame));
    }

    /// <summary>
    /// Dash the player in the direction given
    /// </summary>
    /// <param name="aDir">Direction that the player is dashing in</param>
    /// <param name="aLength">Length of the dash in units</param>
    /// <param name="aDuration">Duration of dash</param>
    /// <param name="aDelay">Delay at the end of the dash</param>
    public void attackDash(Vector2 aDir, float aLength, float aDuration = 0.5f, float aDelay = 1f)
    {
        //Get the layer of everything except for the player
        LayerMask everythingExceptPlayer = LayerMaskExtension.getElse("Player");

        RaycastHit2D[] hits = new RaycastHit2D[3];

        //Keep track of which raycasthit, hit the enemey 
        int rayhitIndex = 0;

        BaseUnit enemyUnit = null;

        //Go through each raycast
        for (int i = 0; i < hits.Length; i++)
        {
            //Get the players position
            Vector2 playerPos = transform.position;

            //Create some raycasts in the direction given
            if (aDir == Vector2.right || aDir == Vector2.left)
            {
                Vector2 yDif = new Vector2(0, 1) / 2;
                playerPos -= yDif;
                playerPos += i * yDif;
            }
            else if (aDir == Vector2.up || aDir == Vector2.down)
            {
                Vector2 xDif = new Vector2(1, 0) / 2;
                playerPos -= xDif;
                playerPos += i * xDif;
            }
            else
            {
                //You can only dash in the 4 directions
                throw new System.NotImplementedException();
            }

            //Get the raycast
            hits[i] = Physics2D.Raycast(transform.position, aDir, aLength, everythingExceptPlayer);

            //If the raycast hit something
            if (hits[i].transform != null)
            {
                //And it was the enemy
                if (hits[i].transform.tag == "Enemy")
                {
                    //Track the enemy postion
                    rayhitIndex = i;
                    enemyUnit = hits[i].transform.GetComponent<BaseUnit>();
                    //Break the loop
                    break;
                }
            }
        }

        //If we hit the enemy
        if (enemyUnit != null)
        {
            //If the player is not attacking
            if (!_isAttacking)
            {
                //If an attack is still continuing
                if (_attack != null)
                {
                    //Remove the attack
                    rig2d.gravityScale = _playerGravScale;

                    StopCoroutine(_attack);
                }

                //Get the distance between tthe player and the enemy
                float distBetweenEnemyAndPlayer = Vector2.Distance(transform.position, hits[rayhitIndex].transform.position);

                //Dash at the enemy
                _attack = StartCoroutine(IAttackDash(aDir, distBetweenEnemyAndPlayer + 3f, aDuration, aDelay));

                //Shake the camera in the direction given
                float cameraShake = 5f;
                Camera.main.GetComponent<DirectionalCameraShake>().push(aDir, cameraShake);

                //Destroy the enemy
                enemyUnit.destroy();

            }
        }
    }

    /// <summary>
    /// Reset the player to starting position
    /// </summary>
    private void _resetToStart()
    {
        //Teleport the player back to the starting position
        transform.position = _startingPos;

        //Erase sides
        previousObjectName = currentObjectName = "";

        //Remove the velocity of the player
        rig2d.velocity = Vector2.zero;

        //Set the gravity back to normal
        while (Physics2D.gravity.normalized != Vector2.down)
        {
            _rotateGravity("", true);
        }

        //Reload the level
        ArenaSetup.instance.reloadLevel();

        //Pause the game, then Start the countdown timer
        StartCoroutine(IStartCountDownTimer(true, 3));

    }

    #endregion

    #region Coroutines

    /// <summary>
    /// Start the count down timer
    /// </summary>
    /// <param name="aStartOfGame">Is it the start of the game</param>
    /// <param name="aCountDownTime">Amount of time for the countdown</param>
    public IEnumerator IStartCountDownTimer(bool aStartOfGame, float aCountDownTime = 3)
    {
        //Get the count down text UI
        Text countDownTxt = GameCanvas.instance.countDownUI.transform.GetChild(0).GetComponent<Text>();

        //Hide the pause button
        GameCanvas.instance.displayPauseButton(false);

        //Make the player unable to move
        _canMove = false;

        //While the count down timer is counting down
        while (aCountDownTime > 0)
        {
            //Delay 
            aCountDownTime -= Time.unscaledDeltaTime;

            if (aCountDownTime < 0)
            {
                aCountDownTime = 0;
            }


            countDownTxt.text = ((int)(aCountDownTime + 1)).ToString();

            yield return new WaitForEndOfFrame();
        }

        //Hide the text
        countDownTxt.text = "";

        //The player can move
        _canMove = true;


        //If its during the game
        if(!aStartOfGame)
        {
            //Unpause the game
            TimeController.pause();
        }

        //Display the pause button
        GameCanvas.instance.displayPauseButton(true);

    }

    /// <summary>
    /// Start the attach dash
    /// </summary>
    /// <param name="aDir">The direction of the dash</param>
    /// <param name="aLength">Length of the dash</param>
    /// <param name="aDuration">Duration time of the dash</param>
    /// <param name="aDelay">Delay time at the end of the dash</param>
    /// <returns></returns>
    public IEnumerator IAttackDash(Vector2 aDir, float aLength, float aDuration, float aDelay)
    {
        //Start the attack
        _isAttacking = true;

        //End position of player
        Vector2 endPos = (Vector2)transform.position + (aDir * aLength);

        //Turn off the players gravity
        _playerGravScale = rig2d.gravityScale;
        rig2d.gravityScale = 0;
        rig2d.simulated = false;



        //Move the player towards the position
        float time = 0;
        while (time < 1)
        {
            time += Time.deltaTime / aDuration;
            transform.position = Vector2.Lerp(transform.position, endPos, time);

            yield return new WaitForEndOfFrame();
        }

        //End the attack
        _isAttacking = false;

        //Delay the player at the end of the attack
        yield return new WaitForSeconds(aDelay);

        //Turn on the players gravity
        rig2d.gravityScale = _playerGravScale;
        rig2d.simulated = true;
    }

    #endregion

    #region Unity Functions
    void Start()
    {
        //There can only be one instance of the player
        if (instance == null)
        {
            instance = this;
            playerHealth = GetComponent<PlayerHealth>();
        }

        //Set the gravity back to normal
        while (Physics2D.gravity.normalized != Vector2.down)
        {
            _rotateGravity("", true);
        }

        //Get the player's starting position
        _startingPos = transform.position;
    }

    void Update()
    {
        //The player should always be moving
        _move();

    }

    void OnCollisionEnter2D(Collision2D coll2d)
    {
        //If the player collides with a dangerous object
        if (coll2d.transform.tag.Equals("Danger"))
        {
            //Take damage
            playerHealth.takeDamage();

            //Reset player to starting position
            _resetToStart();
        }
        else
        {
            _checkForGravityRotation(coll2d);
        }
    }

    void OnCollisionStay2D(Collision2D coll2d)
    {
        _isGrounded = true;
    }

    void OnCollisionExit2D(Collision2D coll2d)
    {
        _isGrounded = false;
    }
    #endregion
}
