﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Display the level being played
/// </summary>
public class LevelDisplay : MonoBehaviour {

    #region Functions
    /// <summary>
    /// Display the current level at the top right corner of the screen
    /// </summary>
    private void _displayCurrentLevel()
    {
        //The screen width and screen height
        int width = Screen.width, height = Screen.height;

        //Get the GUIStyle
        GUIStyle style = new GUIStyle();

        //Display the current level at the top right corner of the screen
        Rect rect = new Rect(0, 0, width, height * 2 / 100);
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = height * 2 / 100;
        style.normal.textColor = Color.black;
        string text = string.Format("Level : {0}", "FIX UP");
        GUI.Label(rect, text, style);
    }
    #endregion

    /// <summary>
    /// Display on the GUI
    /// </summary>
    void OnGUI()
    {
        _displayCurrentLevel();
    }
}
