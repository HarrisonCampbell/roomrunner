﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ArenaSetup))]
public class ArenaSetupEditor : Editor {

    #region Variables
    
    /// <summary>
    /// The current arena
    /// </summary>
    private static ArenaSetup _arenaSetup
    {
        get
        {
            return ArenaSetup.instance;
        }
    }

    #endregion

    #region Functions

    /// <summary>
    /// Load the selected level
    /// </summary>
    /// <param name="aLevelIndex">Level to load</param>
    public static void loadLevel(int aLevelIndex)
    {
        _arenaSetup.currentLevel = aLevelIndex;
        _arenaSetup.loadNewLevelEditor();
    }

    #endregion

    /// <summary>
    /// Save the level
    /// </summary>
    public static void saveLevel()
    {
        //Try and save tghe level
        SaveManager.NewSaveState newSave = _arenaSetup.saveLevel();

        //If the save was unsuccessful
        if (!newSave.saveSuccesful)
        {
            //If the level was already deleted
            if (newSave.type == SaveManager.NewSaveState.UnsuccesfulType.DELETED)
            {
                AgreementPopUp.create(
                    new Vector2(600, 300),
                    "Level was previously deleted",
                    newSave.levelSaveData,
                    () => Debug.LogWarning("NOT YET IMPLEMENTED")
                );
            }
            //If the level was a duplicate
            else if (newSave.type == SaveManager.NewSaveState.UnsuccesfulType.DUPLICATE)
            {
                NoticePopUp.create(
                        new Vector2(300, 150),
                        "Duplicate Level",
                        "This level was previously saved"
                );
            }
        }

        //When a setup is saved refresh Unity
        AssetDatabase.Refresh();
    }

    /// <summary>
    /// Delete level
    /// </summary>
    public static void deleteLevel()
    {
        //If the arena is in a clean state (currently loaded a level)
        if (!ArenaSetup.instance.isDirty)
        {
            //Make sure that the level that is being deleted is not in a set
            if (!SaveManager.instance.isLevelInASet(ArenaSetup.instance.currentLevel))
            {

                //Create a text popup window, and describe why you want to delete the level
                TextPopUp.create(
                    new Vector2(600, 300),
                    "Reason for deleting level",
                    (aText) => AgreementPopUp.create(
                                    new Vector2(600, 300),
                                    "Are you sure?",
                                    "If you delete this level it will be removed from the game",
                                    () => ArenaSetup.instance.deleteLevel(aText)
                                )
                    );


                //When a level is deleted, refresh Unity
                AssetDatabase.Refresh();
            }
            else
            {
                //If there is a level in a set, then notify the player that the level cannot be deleted yet until all sets that 
                //contain the level are deleted
                NoticePopUp.create(
                    new Vector2(600, 200),
                    "Cannot Delete Level",
                    "The Level that you are trying to delete is in a set,\n delete the set before deleting the level"
                    );
            }
        }
        else
        {
            //Else create a notice, notifying the user that the save can't be done
            NoticePopUp.create(
                new Vector2(400,200),
                "Non-Saveable State",
                "You have not selected a level correctly, thus deleting a level cannot \nbe done."
                );
        }
    }

    /// <summary>
    /// Reset the level
    /// </summary>
    public static void resetLevel()
    {
        _arenaSetup.resetSetup();
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        //Draw the buttons for the inspector
        if (GUILayout.Button("Load Level"))
        {
            loadLevel(_arenaSetup.currentLevel);
        }

        if (GUILayout.Button("Save Level"))
        {
            saveLevel();
        }

        if (GUILayout.Button("Delete Level"))
        {
            deleteLevel();
        }

        if (GUILayout.Button("Reset"))
        {
            resetLevel();
            AssetDatabase.Refresh();
        }

        
    }
}
