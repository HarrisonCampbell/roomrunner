﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExtensionClasses.Inputs.Mobile;

/// <summary>
/// How the player is being controlled
/// </summary>
[RequireComponent(typeof(PlayerBehaviour))]
public class PlayerController : BaseMobileControls
{

    #region Variables
    /// <summary>
    /// The behaviour that will control the player
    /// </summary>
    private PlayerBehaviour _behaviour;

    /// <summary>
    /// The behaviour that controls the player
    /// </summary>
    public PlayerBehaviour behaviour
    {
        get
        {
            //If there is no PlayerBehaviour attached
            if(_behaviour == null)
            {
                //Create it
                _behaviour = GetComponent<PlayerBehaviour>();
            }

            return _behaviour;
        }
    }

    /// <summary>
    /// Distance of the dash
    /// </summary>
    public float dashDistance = 9f;

    /// <summary>
    /// Duration time of the dash
    /// </summary>
    public float dashDuration = 1f;

    /// <summary>
    /// The time that the player will be delayed after the dash is done
    /// </summary>
    public float dashDelay = 1f;

    #endregion

    #region Functions
    private void _setupControls()
    {
        //Set up the controls
        swipeUp[0] += () => behaviour.attackDash(Vector2.up, dashDistance, dashDuration, dashDelay);
        swipeDown[0] += () => behaviour.attackDash(Vector2.down, dashDistance, dashDuration, dashDelay);
        swipeLeft[0] += () => behaviour.attackDash(Vector2.left, dashDistance, dashDuration, dashDelay);
        swipeRight[0] += () => behaviour.attackDash(Vector2.right, dashDistance, dashDuration, dashDelay);
        screenPressed[0] += () => behaviour.jump();
    }
    #endregion

    void Start()
    {
        _setupControls();
    }


}

