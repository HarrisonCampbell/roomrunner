﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom inspector for the WorldSelectTemplate
/// </summary>
[CustomEditor(typeof(WorldSelectTemplate))]
public class WorldSelectTemplateCustomInspector : Editor
{
    /// <summary>
    /// Get the current script
    /// </summary>
    private WorldSelectTemplate _current
    {
        get
        {
            return (WorldSelectTemplate)target;
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        //Display world editor window
        if (GUILayout.Button("View World levels"))
        {
            //Display the window
            WorldTemplateCreatorWindow.init(_current);
        } 
    }
}
