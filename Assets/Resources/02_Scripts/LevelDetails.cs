﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The details of the level that will be loaded
/// </summary>
public class LevelDetails : MonoBehaviour {

    #region Variables

    /// <summary>
    /// Check if the LevelDetails are in the scene
    /// </summary>
    public static bool activeInScene
    {
        get
        {
            return FindObjectOfType<LevelDetails>() != null;
        }
    }

    /// <summary>
    /// The level structure that we want to load
    /// </summary>
    public LevelStructure levelStruct;

    /// <summary>
    /// The text to display
    /// </summary>
    private Text _contentText;

    /// <summary>
    /// The title of the level
    /// </summary>
    private Text _titleText;

    #endregion

    #region Functions

    /// <summary>
    /// Go Back
    /// </summary>
    public void back()
    {
        //Destroy the details object
        Destroy(gameObject);
    }

    /// <summary>
    /// Play Level
    /// </summary>
    public void playLevel()
    {
        GameManagement.loadLevel(levelStruct);
        Debug.Log("Play Level");
    }
    #endregion

    #region Unity_Functions

    void Start()
    {
        //Get the components
        _contentText = transform.Find("Content/Text").GetComponent<Text>();
        _titleText = transform.Find("Title").GetComponent<Text>();

        //Set the text based on the game mode
        switch (levelStruct.gameType)
        {
            case GameState.GameType.LevelSet:
                _contentText.text = string.Format("Complete a set of levels with {0} lives", levelStruct.lives);
                break;
            case GameState.GameType.RandomLevels:
                _contentText.text = string.Format("Complete {0} levels with {1} lives", levelStruct.levelPrefix, levelStruct.lives);
                break;
            default:
                throw new System.NotImplementedException(levelStruct.gameType.ToString() + " mode has not been implemented");
        }
        _titleText.text = levelStruct.gameType.ToString();
    }

    #endregion
}
