﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// A window that shows all of the levels created
/// </summary>
public class AllLevelsEditorWindow : EditorWindow
{
    #region Variables

    /// <summary>
    /// Minimum tile size allowed
    /// </summary>
    private int _minTileSize = 10;

    /// <summary>
    /// Maximum tile size allowed
    /// </summary>
    private int _maxTileSize = 20;

    /// <summary>
    /// Current tile size allowed
    /// </summary>
    private int _tileSize = 10;

    /// <summary>
    /// Scroll position of the window
    /// </summary>
    private Vector2 _scrollPos;

    /// <summary>
    /// Level selected
    /// </summary>
    private int _levelSelected;

    /// <summary>
    /// Size of the arena relative to the coloums and rows
    /// </summary>
    private Vector2 _sizeOfArena
    {
        get
        {
            return new Vector2((ArenaLevelEditor.totalHorizontalPillars + 2), (ArenaLevelEditor.totalVerticalPillars + 2)) * _tileSize;
        }
    }

    #endregion

    #region Functions

    /// <summary>
    /// Controls for the window
    /// </summary>
    private void _controls()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("TileSize : " + _tileSize);
        //Set the tile size
        _tileSize = (int)GUILayout.HorizontalSlider(_tileSize, _minTileSize, _maxTileSize);
        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// Displays all of the levels
    /// </summary>
    private void _displayAllLevels()
    {
        //Number of levels
        int numberOfLevels = SaveManager.instance.allPossibleLevels.Length;

        //Number of arenas that can fit horizontally, and vertically
        int xNumArena = (int)(position.width / _sizeOfArena.x);
        int yNumArena = (numberOfLevels / xNumArena);
        if (numberOfLevels % xNumArena != 0)
        {
            yNumArena++;
        }

        //Start the scrolling
        _scrollPos = GUILayout.BeginScrollView(_scrollPos);

        //Area that is needed to view all of the arenas
        GUILayout.Space(yNumArena * _sizeOfArena.y);

        //Display each arena in the window
        for (int i = 0; i < numberOfLevels - 1; i++)
        {
            Vector2 pos = new Vector2(
                (((i % xNumArena) * (ArenaLevelEditor.totalHorizontalPillars + 2)) * _tileSize),
                (((i / xNumArena) * (ArenaLevelEditor.totalVerticalPillars + 2)) * _tileSize)
            );

            ArenaDrawingTools.drawArena(i, pos, Vector2.one * _tileSize);

            //If the arena is in the scene
            if (ArenaSetup.inScene)
            {
                //Highlight the arena that is currently selected
                if (ArenaSetup.instance.currentLevel == i)
                {
                    UnityEditorDrawingTools.drawRect(pos, _sizeOfArena, new Color(0, 0.5f, 1f, 0.5f));
                }

            }
        }


        GUILayout.EndScrollView();

        //Handles selecting the active arena
        _selectArena(xNumArena);
    }

    /// <summary>
    /// Handles selecting the arena
    /// </summary>
    /// <param name="aXNumArena">Number of arenas that can be fit horizontally</param>
    private void _selectArena(int aXNumArena)
    {
        //If the arena is in the scene
        if (ArenaSetup.inScene)
        {
            //Then get the level that is currentlu selected
            _levelSelected = ArenaSetup.instance.currentLevel;

            //If the user clicks on the window
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                //Then get the arena selected 
                Vector2 arenaSelected = new Vector2(
                    (Event.current.mousePosition.x + _scrollPos.x) / (_sizeOfArena.x),
                    (Event.current.mousePosition.y + _scrollPos.y) / (_sizeOfArena.y)
                    );
                arenaSelected = new Vector2(Mathf.FloorToInt(arenaSelected.x), Mathf.FloorToInt(arenaSelected.y));

                //And load it into the scene
                _levelSelected = (int)(arenaSelected.x + arenaSelected.y * aXNumArena);
                ArenaSetupEditor.loadLevel(_levelSelected);
                Repaint();
            }
        }
    }

    /// <summary>
    /// Adds window that allows the player to view all of the levels
    /// </summary>
    [MenuItem("Custom Tools/Level Creation/All Level Selector")]
    private static void _init()
    {
        // Get existing open window or if none, make a new one:
        AllLevelsEditorWindow window = (AllLevelsEditorWindow)GetWindow(typeof(AllLevelsEditorWindow));
        window.Show();
    }

    #endregion

    #region Unity_Functions

    void OnGUI()
    {
        _controls();
        //Display all the levels
        _displayAllLevels();
    }

    #endregion

}
