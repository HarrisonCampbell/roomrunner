﻿using UnityEngine;

/// <summary>
/// Keeps track of the score
/// </summary>
public abstract class ScoreManager{

    #region Variables
    /// <summary>
    /// Current score, can only be set internally
    /// </summary>
    private int _score = 0;

    /// <summary>
    /// Current Score
    /// </summary>
    public int score
    {
        get
        {
            return _score;
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// Add points to the score
    /// </summary>
    /// <param name="aPoints">Number of points to add</param>
    public virtual void addPoints(int aPoints)
    {
        if(aPoints < 0)
        {
            throw new System.Exception("Points need to be 0 or higher");
        }

        _score += aPoints;
    }

    /// <summary>
    /// Remove points to the score
    /// </summary>
    /// <param name="aPoints">Number of points to remove</param>
    public virtual void removePoints(int aPoints)
    {
        if (aPoints < 0)
        {
            throw new System.Exception("Points need to be 0 or higher");
        }

        _score -= aPoints;
    }

    /// <summary>
    /// Add a point to the score
    /// </summary>
    public virtual void addPoint()
    {
        addPoints(1);
    }

    /// <summary>
    /// Remove a point from the score
    /// </summary>
    public virtual void RemovePoint()
    {
        removePoints(1);
    }

    /// <summary>
    /// Reset the score
    /// </summary>
    public virtual void resetScore()
    {
        _score = 0;
    }
    #endregion
}
