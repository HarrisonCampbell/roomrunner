﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using ExtensionClasses.Time;

/// <summary>
/// Handles all animations to the health bar UI
/// </summary>
public class HealthBarUIController : MonoBehaviour {

    #region Variables

    /// <summary>
    /// Health bar animations
    /// </summary>
    private Coroutine[] _healthBarThreads;

    #endregion

    #region Functions

    /// <summary>
    /// Set the UI position
    /// </summary>
    /// <param name="aMaxHealth">Max health of the player</param>
    private void _setUIPosition(int aMaxHealth)
    {
        //Go through the excess health bars, and hide them
        for (int i = aMaxHealth; i < transform.childCount; i++)
        {
            GameObject healthBar = transform.GetChild(i).gameObject;
            healthBar.SetActive(false);
        }

        //How much the healthbar needs to be shifted
        float shiftHealthBarX = (aMaxHealth - transform.childCount) * 45f;
        transform.localPosition -= new Vector3(shiftHealthBarX, 0);
    }

    /// <summary>
    /// Deplete the health of the selected bar
    /// </summary>
    /// <param name="aHealthIndex">Selected health bar</param>
    public void depleteHealthBar(int aHealthIndex)
    {
        //If there is a healthbar animation running, stop it
        if (_healthBarThreads[aHealthIndex] != null)
        {
            StopCoroutine(_healthBarThreads[aHealthIndex]);
        }

        //Deplete the health bar
        _healthBarThreads[aHealthIndex] = StartCoroutine(IDepleteHealthBar(aHealthIndex));
    }

    /// <summary>
    /// Gain a health bar
    /// </summary>
    /// <param name="aHealthIndex">Selected health bar</param>
    public void gainHealthBar(int aHealthIndex)
    {
        //If there is a healthbar animation running, stop it
        if (_healthBarThreads[aHealthIndex] != null)
        {
            StopCoroutine(_healthBarThreads[aHealthIndex]);
        }

        //Fill the health bar
        _healthBarThreads[aHealthIndex] = StartCoroutine(IGainHealthBar(aHealthIndex));
    }

    #endregion

    #region Coroutine

    /// <summary>
    /// Remove health from bar
    /// </summary>
    /// <returns>Thread of the animation</returns>
    IEnumerator IDepleteHealthBar(int aHealthIndex)
    {
        //Get the healthfill image
        Image healthFill = transform.GetChild(aHealthIndex).GetChild(0).GetComponent<Image>();

        //While the health bar is still not empty
        while (healthFill.fillAmount > 0)
        {
            //deplete it
            healthFill.fillAmount = Mathf.Lerp(healthFill.fillAmount, 0, TimeController.unscaledDeltaTime);

            yield return new WaitForEndOfFrame();
        }

        //Remove the thread
        _healthBarThreads[aHealthIndex] = null;
    }

    /// <summary>
    /// Gain a health bar
    /// </summary>
    /// <returns>Thread of the animation</returns>
    IEnumerator IGainHealthBar(int aHealthIndex)
    {
        //Get the healthfill image

        Image healthFill = transform.GetChild(aHealthIndex).GetChild(0).GetComponent<Image>();

        //While the health bar is still not full
        while (healthFill.fillAmount < 1)
        {
            //fill it
            healthFill.fillAmount = Mathf.Lerp(healthFill.fillAmount, 1, TimeController.unscaledDeltaTime);

            yield return new WaitForEndOfFrame();
        }

        //Remove the thread
        _healthBarThreads[aHealthIndex] = null;
    }

    #endregion

    #region Unity_Functions

    void Start()
    {
        //Set the health bar relative to the players health
        _healthBarThreads = new Coroutine[PlayerHealth.maxHealth];
        //Set the healthbar UI position based on the max health
        _setUIPosition(PlayerHealth.maxHealth);
    }

    #endregion
}
