﻿using System.Collections;
using UnityEngine;
using UnityEditor;

/// <summary>
/// PillarSetup custom inspector
/// </summary>
[CustomEditor(typeof(PillarSetup)),CanEditMultipleObjects]
public class PillarSetupCustomInspector : Editor {

    #region Functions

    /// <summary>
    /// Lower the pillar in the <see cref="PillarSetup"/>
    /// </summary>
    private void _lowerPillar()
    {
        //Get all of the objects selected
        GameObject[] objsSelected = Selection.gameObjects;

        //Go through each selected object
        foreach (GameObject gameObj in objsSelected)
        {
            //If there is a PillarSetup script attached, then lower the pillar
            PillarSetup pillarSetup = gameObj.GetComponent<PillarSetup>();
            if (pillarSetup != null)
            {
                pillarSetup.lower();
                pillarSetup.moveToSetHeight();
            }
        }
    }

    /// <summary>
    /// Raise the pillar in the <see cref="PillarSetup"/>
    /// </summary>
    private void _raisePillar()
    {
        //Get all of the objects selected
        GameObject[] objsSelected = Selection.gameObjects;

        //Go through each selected object
        foreach (GameObject gameObj in objsSelected)
        {
            //If there is a PillarSetup script attached, then raise the pillar
            PillarSetup pillarSetup = gameObj.GetComponent<PillarSetup>();
            if (pillarSetup != null)
            {
                pillarSetup.raise();
                pillarSetup.moveToSetHeight();
            }
        }
    }

    #endregion

    public override void OnInspectorGUI()
    {
        //Get the selected pillar
        PillarSetup tarPillarSetup = (PillarSetup)target;
        //Display the pillar level
        EditorGUILayout.LabelField("Pillar level : " + tarPillarSetup.pillarLevel.ToString());

        //If this button is pressed, raise the pillar
        if (GUILayout.Button("Raise Level"))
        {
            _raisePillar();
        }

        //If this button is pressed, lower the pillar
        if (GUILayout.Button("Lower Level"))
        {
            _lowerPillar();
        }

        //Draw the default values
        DrawDefaultInspector();
    }
}
