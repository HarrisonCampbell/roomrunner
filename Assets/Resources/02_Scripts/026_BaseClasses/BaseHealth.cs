﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExtensionClasses.Exceptions;

/// <summary>
/// Standard base health for all health components
/// </summary>
public abstract class BaseHealth : MonoBehaviour
{
    #region Variables

    /// <summary>
    /// Max health of component
    /// </summary>
    public int maxHealth = 3;

    /// <summary>
    /// Current health of component
    /// </summary>
    private int _currentHealth;

    /// <summary>
    /// Current health of component
    /// </summary>
    public int currentHealth
    {
        get
        {
            return _currentHealth;
        }
    }

    /// <summary>
    /// Is there still some health left 
    /// </summary>
    public bool isAlive
    {
        get
        {
            //Return true if current health is greater than 0, false otherwise
            return _currentHealth > 0;
        }
    }

    #endregion

    #region Functions

    /// <summary>
    /// How much health is going to be lost
    /// </summary>
    /// <param name="aDmg">Amount of damage taken</param>
    public virtual void takeDamage(int aDmg = 1)
    {
        //If the damage is less than or equal to 0
        if (aDmg <= 0)
        {
            //Throw an InvalidDamageException
            throw new InvalidDamageException();
        }

        //Else deal damage
        _currentHealth = (int)Mathf.Clamp(_currentHealth - aDmg, 0, maxHealth);
    }

    /// <summary>
    /// How much health will be restored
    /// </summary>
    /// <param name="healthRestored">Amount of health restored</param>
    public virtual void heal(int aHealthRestored = 1)
    {
        //If the health restored is less than or equal to 0
        if (aHealthRestored <= 0)
        {
            //This is invalid thus throw an exception
            throw new InvalidHealException();
        }

        //Else heal
        _currentHealth = (int)Mathf.Clamp(_currentHealth + aHealthRestored, 0, maxHealth);

    }

    /// <summary>
    /// Reset the health to the max
    /// </summary>
    public void resetHealth()
    {
        _currentHealth = maxHealth;
    }
    #endregion
}
