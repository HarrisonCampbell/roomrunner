﻿using UnityEngine;
using UnityEditor;


/// <summary>
/// Level editor tool for the arena
/// </summary>
public class ArenaLevelEditor : EditorWindow
{
    #region Variables

    /// <summary>
    /// Total number of horizontal pillars
    /// </summary>
    public static int totalHorizontalPillars
    {
        get
        {
            return 9;
        }
    }

    /// <summary>
    /// Total number of vertical pillars
    /// </summary>
    public static int totalVerticalPillars
    {
        get
        {
            return 13;
        }
    }

    /// <summary>
    /// Size of each tile
    /// </summary>
    private float _tileSize = 30f;

    /// <summary>
    /// Minimum tile size
    /// </summary>
    private float _minTileSize = 15f;

    /// <summary>
    /// Maximum tile size
    /// </summary>
    private float _maxTileSize = 30f;

    /// <summary>
    /// Scroll position of the window
    /// </summary>
    private Vector2 _scrollPos;

    /// <summary>
    /// ???
    /// </summary>
    private readonly int _butHeight = 40;

    /// <summary>
    /// The mouse position relative to the cell in the arena
    /// </summary>
    private Vector2 mousePosRelativeToArena
    {
        get
        {
            Vector2 mousePos = (Event.current.mousePosition - Vector2.one * _tileSize * 3) - new Vector2(5, _tileSize);
            mousePos = (mousePos - new Vector2(0, _butHeight));
            return new Vector2(Mathf.Floor(mousePos.x / _tileSize), Mathf.Floor(mousePos.y / _tileSize));
        }
    }

    #endregion

    #region Functions

    /// <summary>
    /// Create the Arena Level Editor from the Unity Menu
    /// </summary>
    [MenuItem("Custom Tools/Level Creation/Arena Level Editor")]
    private static void _init()
    {
        // Get existing open window or if none, make a new one:
        ArenaLevelEditor window = (ArenaLevelEditor)GetWindow(typeof(ArenaLevelEditor));
        window.Show();
    }

    /// <summary>
    /// Controllers used to control the height of the pillar
    /// </summary>
    /// <param name="aPillarSetup">Pillar to move</param>
    /// <param name="aButtonPos">Button position</param>
    /// <param name="aButtonSize">Button Size</param>
    /// <param name="aSide">Side of the arena</param>
    private void _pillarAdjustmentControllers(PillarSetup aPillarSetup, Vector2 aButtonPos, Vector2 aButtonSize, ArenaSetup.ArenaSide aSide)
    {
        //Position of the buttons
        Vector2 riseButtonPos = Vector2.zero, lowerButtonPos = Vector2.zero;
        //Labels of the buttons
        string lowerButtonLabel = "--Change--", riseButtonLabel = "--Change--";

        //Label the buttons, and align them depending on what side they are going to represent
        if (aSide == ArenaSetup.ArenaSide.EAST)
        {
            lowerButtonPos = aButtonPos + new Vector2(aButtonSize.x, 0);
            riseButtonPos = aButtonPos;
            lowerButtonLabel = "→";
            riseButtonLabel = "←";
        }
        else if (aSide == ArenaSetup.ArenaSide.WEST)
        {
            lowerButtonPos = aButtonPos;
            riseButtonPos = aButtonPos + new Vector2(aButtonSize.x, 0);
            lowerButtonLabel = "←";
            riseButtonLabel = "→";
        }
        else if (aSide == ArenaSetup.ArenaSide.NORTH)
        {
            lowerButtonPos = aButtonPos;
            riseButtonPos = aButtonPos + new Vector2(0, aButtonSize.y);
            lowerButtonLabel = "↑";
            riseButtonLabel = "↓";
        }
        else if (aSide == ArenaSetup.ArenaSide.SOUTH)
        {
            lowerButtonPos = aButtonPos + new Vector2(0, aButtonSize.y);
            riseButtonPos = aButtonPos;
            lowerButtonLabel = "↓";
            riseButtonLabel = "↑";
        }

        //Create the button that will lower the pillar
        GUILayout.BeginArea(new Rect(lowerButtonPos, aButtonSize));
        if (GUILayout.Button(lowerButtonLabel, new GUILayoutOption[] { GUILayout.Width(aButtonSize.x), GUILayout.Height(aButtonSize.y) }))
        {
            aPillarSetup.lower();
            aPillarSetup.moveToSetHeight();
            Repaint();
        }
        GUILayout.EndArea();

        //Create the button that will raise the pillar
        GUILayout.BeginArea(new Rect(riseButtonPos, aButtonSize));
        if (GUILayout.Button(riseButtonLabel, new GUILayoutOption[] { GUILayout.Width(aButtonSize.x), GUILayout.Height(aButtonSize.y) }))
        {
            aPillarSetup.raise();
            aPillarSetup.moveToSetHeight();
            Repaint();
        }
        GUILayout.EndArea();
    }

    /// <summary>
    /// Add an item to the arena
    /// </summary>
    private void _addEntities()
    {

        //If the mouse pos is in the arena
        if ((mousePosRelativeToArena.x >= 0 && mousePosRelativeToArena.x < totalHorizontalPillars) &&
            (mousePosRelativeToArena.y >= 0 && mousePosRelativeToArena.y < totalVerticalPillars))
        {
            //If the item has not already been placed in the arena
            if (!ArenaSetup.instance.itemPlacements.ContainsKey(mousePosRelativeToArena))
            {
                //Create a CellDetails with the item selected, and the unit that holds it
                ArenaSetup.CellDetails cellDet = new ArenaSetup.CellDetails();

                //Place the object if the type has been implemented
                if (ArenaSetup.instance.itemSelected == ArenaSetup.Item.ENEMY)
                {
                    BaseObject enemyObj = BaseUnit.create((int)mousePosRelativeToArena.x, (int)mousePosRelativeToArena.y).GetComponent<BaseObject>();
                    cellDet = ArenaSetup.CellDetails.createCellDetails(ArenaSetup.Item.ENEMY, enemyObj);
                }
                else
                {
                    Debug.LogError("Not yet Implemented");
                }
                ArenaSetup.instance.itemPlacements[mousePosRelativeToArena] = cellDet;

                //Set the arena to a dirty state
                ArenaSetup.instance.setToDirty();

                Repaint();
            }
        }


    }

    /// <summary>
    /// Delete an item from the arena
    /// </summary>
    private void _deleteEntities()
    {
        //If the mouse pos is in
        if ((mousePosRelativeToArena.x >= 0 && mousePosRelativeToArena.x < totalHorizontalPillars) &&
            (mousePosRelativeToArena.y >= 0 && mousePosRelativeToArena.y < totalVerticalPillars))
        {
            //Delete the item if its in the Arena
            if (ArenaSetup.instance.itemPlacements.ContainsKey(mousePosRelativeToArena))
            {
                ArenaSetup.instance.itemPlacements[mousePosRelativeToArena].baseObj.destroy();
                ArenaSetup.instance.itemPlacements.Remove(mousePosRelativeToArena);
                Repaint();
            }
        }

    }

    /// <summary>
    /// Draw the items in the arena
    /// </summary>
    /// <param name="aPos">Position of the arena</param>
    /// <param name="aTileSize">Size of the tiles</param>
    private void _drawEntities(Vector2 aPos, Vector2 aTileSize)
    {
        //Fo through each cell
        for (int i = 0; i < totalHorizontalPillars; i++)
        {
            for (int j = 0; j < totalVerticalPillars; j++)
            {
                //If the items list been created
                if (ArenaSetup.instance.itemPlacements != null)
                {
                    //If there is an item in the cell
                    Vector2 vecKey = new Vector2(i, j);
                    if (ArenaSetup.instance.itemPlacements.ContainsKey(vecKey))
                    {
                        //Default color is white if there are no items, else apply a color depending
                        //on the item
                        Color col = Color.white;
                        if (ArenaSetup.instance.itemPlacements[vecKey].item == ArenaSetup.Item.ENEMY)
                        {
                            col = Color.magenta;
                        }
                        else
                        {
                            throw new System.NotImplementedException("Object type is not supported");
                        }
                        //Draw the rectangle
                        ArenaDrawingTools.drawRect(aPos + new Vector2(i * aTileSize.x, j * aTileSize.y), aTileSize, col);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Draw the pillars of the arena
    /// </summary>
    /// <param name="aPos"></param>
    /// <param name="aTileSize"></param>
    private void _drawPillars(Vector2 aPos, Vector2 aTileSize)
    {
        GameObject sides = GameObject.Find("Arena/Sides");
        GameObject eastSide = sides.transform.Find("EastSide").gameObject;
        GameObject northSide = sides.transform.Find("NorthSide").gameObject;
        GameObject westSide = sides.transform.Find("WestSide").gameObject;
        GameObject southSide = sides.transform.Find("SouthSide").gameObject;

        //Set the west side
        for (int i = 0; i < westSide.transform.childCount; i++)
        {
            PillarSetup pillar = westSide.transform.GetChild(i).GetComponent<PillarSetup>();
            Vector2 buttonPos = aPos + new Vector2(0, aTileSize.y * 3 + i * aTileSize.y);

            _pillarAdjustmentControllers(pillar, buttonPos, aTileSize, ArenaSetup.ArenaSide.WEST);

            for (int j = 0; j <= pillar.pillarLevel; j++)
            {
                Vector2 newPos = new Vector2((j * aTileSize.x), aTileSize.y + (i * aTileSize.y));
                ArenaDrawingTools.drawRect(newPos + aPos + aTileSize * 2, aTileSize, Color.Lerp(Color.blue, Color.red, 0.25f));
            }
        }

        //Set the east side
        for (int i = 0; i < eastSide.transform.childCount; i++)
        {
            PillarSetup pillar = eastSide.transform.GetChild(i).GetComponent<PillarSetup>();

            Vector2 buttonPos = aPos + new Vector2((aTileSize.x * (northSide.transform.childCount + 4)), (aTileSize.y * (eastSide.transform.childCount + 2)) - i * aTileSize.y);

            _pillarAdjustmentControllers(pillar, buttonPos, aTileSize, ArenaSetup.ArenaSide.EAST);

            for (int j = 0; j <= pillar.pillarLevel; j++)
            {
                Vector2 newPos = new Vector2((aTileSize.x * (northSide.transform.childCount + 1)) - (j * aTileSize.x), aTileSize.y - (i * aTileSize.y) + (aTileSize.y * (eastSide.transform.childCount - 1)));
                ArenaDrawingTools.drawRect(newPos + aPos + aTileSize * 2, aTileSize, Color.Lerp(Color.blue, Color.red, 0.75f));
            }
        }

        //Set the north side
        for (int i = 0; i < northSide.transform.childCount; i++)
        {
            PillarSetup pillar = northSide.transform.GetChild(i).GetComponent<PillarSetup>();

            Vector2 buttonPos = aPos + new Vector2((aTileSize.x * (northSide.transform.childCount + 2)) - (aTileSize.x * (i)), 0);

            _pillarAdjustmentControllers(pillar, buttonPos, aTileSize, ArenaSetup.ArenaSide.NORTH);

            for (int j = 0; j <= pillar.pillarLevel; j++)
            {
                Vector2 newPos = new Vector2((aTileSize.x * (northSide.transform.childCount + 1)) - (aTileSize.x + (i * aTileSize.x)), aTileSize.y * j);
                ArenaDrawingTools.drawRect(newPos + aPos + aTileSize * 2, aTileSize, Color.Lerp(Color.blue, Color.red, 0.5f));
            }
        }

        //Set the south side
        for (int i = 0; i < southSide.transform.childCount; i++)
        {
            PillarSetup pillar = southSide.transform.GetChild(i).GetComponent<PillarSetup>();

            Vector2 buttonPos = aPos + new Vector2((aTileSize.x * (i + 3)), aTileSize.y * (eastSide.transform.childCount + 4));

            _pillarAdjustmentControllers(pillar, buttonPos, aTileSize, ArenaSetup.ArenaSide.SOUTH);


            for (int j = 0; j <= pillar.pillarLevel; j++)
            {
                Vector2 newPos = new Vector2((aTileSize.x + (i * aTileSize.x)), (aTileSize.y * (westSide.transform.childCount + 1)) - (j * aTileSize.y));
                ArenaDrawingTools.drawRect(newPos + aPos + aTileSize * 2, aTileSize, Color.Lerp(Color.blue, Color.red, 1f));
            }
        }
    }

    /// <summary>
    /// Draw the arena in the editor
    /// </summary>
    /// <param name="aArenaPos">Position of the Arena</param>
    /// <param name="aTileSize">Size of the tiles</param>
    private void _drawArena(Vector2 aArenaPos, Vector2 aTileSize)
    {
        //Draw the pillars in the arena
        _drawPillars(aArenaPos, aTileSize);

        //Draw the items in the arena
        _drawEntities(aArenaPos + (Vector2.one * _tileSize * 3), aTileSize);

        //Draw the grid
        for (int i = 0; i < totalHorizontalPillars; i++)
        {
            for (int j = 0; j < totalVerticalPillars; j++)
            {
                Vector2 startGridPos = aArenaPos + aTileSize * 3;
                ArenaDrawingTools.drawRectOutline(startGridPos + new Vector2(i * aTileSize.x, j * aTileSize.y), aTileSize, Color.gray);
            }
        }
    }
    
    /// <summary>
    /// Handles all interactions with the ArenaSetup
    /// </summary>
    private void _displayArenaController()
    {
        GUILayout.BeginHorizontal();
        //Resets the layout
        if (GUILayout.Button("Reset Level"))
        {
            ArenaSetupEditor.resetLevel();
            //When a setup is saved refresh Unity
            AssetDatabase.Refresh();
        }

        //Saves the layout
        if (GUILayout.Button("Save Level"))
        {
            ArenaSetupEditor.saveLevel();
            //When a setup is saved refresh Unity
            AssetDatabase.Refresh();
        }

        //Deletes the layout
        if (GUILayout.Button("Delete Level"))
        {
            ArenaSetupEditor.deleteLevel();
            //When a setup is saved refresh Unity
            AssetDatabase.Refresh();
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        ArenaSetup.instance.itemSelected = (ArenaSetup.Item)EditorGUILayout.EnumPopup("Item to place:", ArenaSetup.instance.itemSelected);

        GUILayout.Label("Level : " + ArenaSetup.instance.currentLevel, GUILayout.Width(60));

        GUILayout.Label("Tile Size : " + _tileSize, GUILayout.MaxWidth(100));
        _tileSize = (int)GUILayout.HorizontalSlider(_tileSize, _minTileSize, _maxTileSize, GUILayout.MinWidth(100));

        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// Update the items in the arena
    /// </summary>
    private void _updateItemsInArena()
    {
        //Get all of the BaseObjects in the game
        BaseObject[] allBaseObjects = FindObjectsOfType<BaseObject>() as BaseObject[];

        //Go through object, and update it in the Arena
        for (int i = 0; i < allBaseObjects.Length; i++)
        {
            ArenaSetup.CellDetails cellDetails = new ArenaSetup.CellDetails();

            if (allBaseObjects[i].GetType() == typeof(BaseUnit))
            {
                cellDetails = ArenaSetup.CellDetails.createCellDetails(ArenaSetup.Item.ENEMY, allBaseObjects[i]);
            }
            else
            {
                Debug.LogWarning("Not yet Implemented");
            }

            Vector2 itemTransPos = allBaseObjects[i].transform.localPosition;
            itemTransPos = new Vector2(itemTransPos.x, -itemTransPos.y);
            if (!ArenaSetup.instance.itemPlacements.ContainsKey(itemTransPos))
            {
                ArenaSetup.instance.itemPlacements.Add(itemTransPos, cellDetails);
            }
            else
            {
                ArenaSetup.instance.itemPlacements[itemTransPos] = cellDetails;
            }

        }

    }

    #endregion

    #region Unity_Functions

    void OnGUI()
    {
        //If the arena setup is in the scene
        if (ArenaSetup.inScene)
        {
            _scrollPos = GUILayout.BeginScrollView(_scrollPos);

            _updateItemsInArena();
            GUILayout.BeginVertical();

            GUILayout.Label("Arena Level Editor", EditorStyles.boldLabel);

            _displayArenaController();

            Vector2 arenaPos = new Vector2(5, 70);

            _drawArena(arenaPos, Vector2.one * _tileSize);

            //Left click
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                //Add an item
                _addEntities();
            }
            //Right click
            else if (Event.current.type == EventType.MouseDown && Event.current.button == 1)
            {
                //Delete an item
                _deleteEntities();
            }

            GUILayout.EndVertical();

            GUILayout.Space(_butHeight + (_tileSize * (ArenaLevelEditor.totalVerticalPillars + 3)));

            GUILayout.EndScrollView();

        }
        else
        {
            GUILayout.Label("Arena is not in the scene", EditorStyles.boldLabel);
        }
    }

    #endregion
}
