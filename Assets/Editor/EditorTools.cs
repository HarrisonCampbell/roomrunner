﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// All drawing tools for the Unity Editor
/// </summary>
public class UnityEditorDrawingTools
{
    #region Functions

    /// <summary>
    /// Draw a rectangle in the window
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="size"></param>
    /// <param name="color"></param>
    public static void drawRect(Vector2 pos, Vector2 size, Color color)
    {
        EditorGUI.DrawRect(new Rect(pos, size), color);
    }

    /// <summary>
    /// Draw a rectangle outline in the window
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="size"></param>
    /// <param name="color"></param>
    /// <param name="outlineWidth"></param>
    public static void drawRectOutline(Vector2 pos, Vector2 size, Color color, float outlineWidth = 1f)
    {
        //LeftSide
        EditorGUI.DrawRect(new Rect(pos, new Vector2(outlineWidth, size.y)), color);

        //TopSide
        EditorGUI.DrawRect(new Rect(pos, new Vector2(size.x, outlineWidth)), color);

        //RightSide
        EditorGUI.DrawRect(new Rect(pos + new Vector2(size.x - outlineWidth, 0), new Vector2(outlineWidth, size.y)), color);

        //BottomSide
        EditorGUI.DrawRect(new Rect(pos + new Vector2(0, size.y - outlineWidth), new Vector2(size.x, outlineWidth)), color);

    }

    #endregion
}

/// <summary>
/// Drawing tools specific to the Arena
/// </summary>
public class ArenaDrawingTools : UnityEditorDrawingTools
{
    #region Functions

    /// <summary>
    /// Draw the grid for the arena
    /// </summary>
    /// <param name="aPos">Position of the arena</param>
    /// <param name="aTileSize">Tile size</param>
    private static void _drawGrid(Vector2 aPos,Vector2 aTileSize)
    {
        for (int i = 0; i < ArenaLevelEditor.totalHorizontalPillars+2; i++)
        {
            for (int j = 0; j < ArenaLevelEditor.totalVerticalPillars+2; j++)
            {
                drawRectOutline(aPos + new Vector2(i, j) * aTileSize.x, Vector2.one * aTileSize.y, Color.gray);
            }
        }
    }

    /// <summary>
    /// Draw the pillars
    /// </summary>
    /// <param name="aPillarLayout">The layout to draw</param>
    /// <param name="aPos">Position of the arena</param>
    /// <param name="aTileSize">Tile size</param>
    private static void _drawPillars(string aPillarLayout, Vector2 aPos, Vector2 aTileSize)
    {
        //For through each side and draw it out
        string[] arenaSides = aPillarLayout.Split(':');
        string[] eastSide = arenaSides[0].Split(',');
        string[] northSide = arenaSides[1].Split(',');
        string[] westSide = arenaSides[2].Split(',');
        string[] southSide = arenaSides[3].Split(',');

        //EastSide
        for (int i = 0; i < eastSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(eastSide[i]) + 1; j++)
            {
                Vector2 pos = aPos + new Vector2(aTileSize.x * (northSide.Length + 1), aTileSize.y * (eastSide.Length - i));
                pos.x -= (int.Parse(eastSide[i]) - j) * aTileSize.x;
                drawRect(pos, aTileSize, Color.red);
            }
        }

        //NorthSide
        for (int i = 0; i < northSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(northSide[i]) + 1; j++)
            {
                Vector2 pos = aPos + new Vector2(aTileSize.x * (northSide.Length - i), 0);
                pos.y += (j) * aTileSize.y;
                drawRect(pos, aTileSize, Color.green);
            }
        }

        //WestSide
        for (int i = 0; i < westSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(westSide[i]) + 1; j++)
            {
                Vector2 pos = aPos + new Vector2(0, aTileSize.y * (i + 1));
                pos.x += (j) * aTileSize.x;
                drawRect(pos, aTileSize, Color.cyan);
            }
        }

        //SouthSide
        for (int i = 0; i < southSide.Length; i++)
        {
            for (int j = 0; j < int.Parse(southSide[i]) + 1; j++)
            {
                Vector2 pos = aPos + new Vector2(aTileSize.x * (i + 1), aTileSize.y * (eastSide.Length + 1));
                pos.y -= (j) * aTileSize.y;
                drawRect(pos, aTileSize, Color.yellow);
            }
        }

    }

    /// <summary>
    /// Draw the items
    /// </summary>
    /// <param name="aItemLayout">The items to draw</param>
    /// <param name="aPos">Position of the arena</param>
    /// <param name="aTileSize">Tile size</param>
    private static void _drawItems(string aItemLayout, Vector2 aPos, Vector2 aTileSize)
    {
        //Get all of the items
        string[] items = aItemLayout.Split(':');

        foreach (string item in items)
        {
            if (!string.IsNullOrEmpty(item))
            {
                //Get the location and the type
                string location = item.Split('|')[0];
                string type = item.Split('|')[1];

                Vector2 pos = new Vector2(int.Parse(location.Split(',')[0]) + 1, int.Parse(location.Split(',')[1]) + 1);
                ArenaSetup.Item arenaItem = (ArenaSetup.Item)System.Enum.Parse(typeof(ArenaSetup.Item), type);

                Color col = Color.white;

                switch (arenaItem)
                {
                    case ArenaSetup.Item.ENEMY:
                        col = Color.magenta;
                        break;
                    default:
                        throw new System.NotImplementedException("Object type not supported");
                }

                drawRect(aPos + new Vector2(pos.x * aTileSize.x, pos.y * aTileSize.y), aTileSize, col);
            }
        }
    }

    /// <summary>
    /// Draw the arena in the editor
    /// </summary>
    /// <param name="aArenaIndex">Arena to draw</param>
    /// <param name="aPos">Position of the arena</param>
    /// <param name="aTileSize">Tile size</param>
    public static void drawArena(int aArenaIndex, Vector2 aPos, Vector2 aTileSize)
    {
        //Get the layout of the index given
        string layout = SaveManager.instance.allPossibleLevels[aArenaIndex];

        //Get the pilar levels, and the item layout
        string pillarLayouts = layout.Split('-')[0];
        string itemLayout = layout.Split('-')[1];

        _drawGrid(aPos, aTileSize);
        _drawPillars(pillarLayouts, aPos, aTileSize);
        if (!string.IsNullOrEmpty(itemLayout))
        {
            _drawItems(itemLayout, aPos, aTileSize);
        }

        //Draw the corners
        drawRect(aPos + new Vector2(0, 0), aTileSize, Color.gray);
        drawRect(aPos + new Vector2(0, (ArenaLevelEditor.totalVerticalPillars + 1) * aTileSize.y), aTileSize, Color.gray);
        drawRect(aPos + new Vector2((ArenaLevelEditor.totalHorizontalPillars + 1) * aTileSize.x, 0), aTileSize, Color.gray);
        drawRect(aPos + new Vector2((ArenaLevelEditor.totalHorizontalPillars + 1) * aTileSize.x, (ArenaLevelEditor.totalVerticalPillars + 1) * aTileSize.y), aTileSize, Color.gray);

        //Draw outline
        drawRectOutline(aPos, new Vector2((ArenaLevelEditor.totalHorizontalPillars + 2) * aTileSize.x, (ArenaLevelEditor.totalVerticalPillars + 2) * aTileSize.y), Color.black);
    }
    
    /// <summary>
    /// Draw the texture of the arena
    /// </summary>
    /// <param name="aGrid">Grid that contains the information of the arena</param>
    /// <param name="aScale">Scale of the drawing</param>
    /// <returns>A texture of the arena</returns>
    public static Texture2D makeArenaTexture(int[,] aGrid, int aScale = 1)
    {
        Color[] pix = new Color[aGrid.GetLength(0) * aGrid.GetLength(1)];
        for (int i = 0; i < aGrid.GetLength(0); i++)
        {
            for (int j = 0; j < aGrid.GetLength(1); j++)
            {
                if (aGrid[i, j] == 0)
                {
                    pix[i + j * aGrid.GetLength(0)] = Color.clear;
                }
                else if (aGrid[i, j] == 1)
                {
                    pix[i + j * aGrid.GetLength(0)] = Color.white;
                }
                else if(aGrid[i,j] == 2)
                {
                    pix[i + j * aGrid.GetLength(0)] = Color.magenta;
                }
                else
                {
                    throw new System.NotImplementedException("Item not supported");
                }
            }
        }

        Texture2D result = new Texture2D(aGrid.GetLength(0), aGrid.GetLength(1));
        result.SetPixels(pix);
        result.Apply();

        TextureScale.Point(result, aGrid.GetLength(0) * aScale, aGrid.GetLength(1) * aScale);

        return result;
    }

    #endregion
}
