﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Loads level
/// </summary>
public class LevelSelectButton : MonoBehaviour {

    #region Variables

    /// <summary>
    /// Test of the button
    /// </summary>
    public Text buttonTxt;

    /// <summary>
    /// What level structure the button will use
    /// </summary>
    public LevelStructure levelStruct;

    #endregion

    #region Functions

    /// <summary>
    /// Load the level
    /// </summary>
    private void _loadLevel()
    {
        //If the level details are not active in the scene
        if (!LevelDetails.activeInScene)
        {
            //Get the level details
            GameObject levelDetails = Instantiate(Resources.Load("01_Prefabs/LevelDetails")) as GameObject;

            //Add the level details to the Main Canvas
            levelDetails.transform.SetParent(GameObject.FindGameObjectWithTag("MainCanvas").transform);
            levelDetails.transform.localPosition = Vector2.zero;
            RectTransform rect = levelDetails.GetComponent<RectTransform>();
            rect.sizeDelta = Vector2.zero;
            rect.localScale = Vector3.one;

            levelDetails.GetComponent<LevelDetails>().levelStruct = levelStruct;
        }
    }

    /// <summary>
    /// Create a LevelSelectButton
    /// </summary>
    /// <param name="aPos">Position of button</param>
    /// <param name="aParent">Parent transform of button</param>
    /// <param name="aLvlNum">Level Number</param>
    /// <returns>New LevelSelectButton</returns>
    public static LevelSelectButton createButton(Vector2 aPos,Transform aParent, int aLvlNum)
    {
        //Create an instance of the button
        GameObject lvlButton = Instantiate(Resources.Load("01_Prefabs/LevelButton"), aParent) as GameObject;
        lvlButton.transform.localPosition = aPos;
        lvlButton.transform.name = aLvlNum.ToString();
        LevelSelectButton lvlSelect = lvlButton.AddComponent<LevelSelectButton>();

        //Get the text of the button, and add the level number
        lvlSelect.buttonTxt = lvlButton.transform.Find("Text").GetComponent<Text>();
        lvlSelect.buttonTxt.text = aLvlNum.ToString();
        
        return lvlSelect;
    }

    void Start()
    {
        //Add a load level listener to the button
        Button button = GetComponent<Button>();
        button.onClick.AddListener(() => _loadLevel());
    }

    #endregion
}

/// <summary>
/// Handles how the level will be structured
/// </summary>
public struct LevelStructure
{

    /// <summary>
    /// Type of level being loaded
    /// </summary>
    public GameState.GameType gameType;

    /// <summary>
    /// A unique number used to load a certain level set
    /// </summary>
    public int levelPrefix;

    /// <summary>
    /// Number of lives the player will have
    /// </summary>
    public int lives;

    /// <summary>
    /// Constructor for LevelStructure
    /// </summary>
    /// <param name="aGameType">What level will be loaded</param>
    /// <param name="aLives">Number of lives the player is allowed</param>
    /// <param name="aNumLvls">A unique number used depending on the game mode</param>
    public LevelStructure(GameState.GameType aGameType,int aLives, int aLevelPrefix)
    {
        this.gameType = aGameType;
        this.lives      = aLives;
        this.levelPrefix = aLevelPrefix;
    }
}
