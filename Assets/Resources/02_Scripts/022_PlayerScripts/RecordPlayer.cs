﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExtensionClasses.Time;

/// <summary>
/// Player variables that will be recorded
/// </summary>
public struct PlayerState
{
    /// <summary>
    /// Player position
    /// </summary>
    public Vector3 position;

    /// <summary>
    /// Default constructor of PlayerState
    /// </summary>
    /// <param name="aPosition">Position of the player</param>
    public PlayerState(Vector3 aPosition)
    {
        this.position = aPosition;
    }
}

/// <summary>
/// Records the player in real time
/// </summary>
public class RecordPlayer : MonoBehaviour
{
    #region Variables

    /// <summary>
    /// Player that is being recorded
    /// </summary>
    public PlayerBehaviour player;

    /// <summary>
    /// How long is the player being recorded for
    /// </summary>
    public int time = 0;

    /// <summary>
    /// Is time being rewinded
    /// </summary>
    public bool rewindTime = false;

    /// <summary>
    /// Speed that the recording is being played at
    /// </summary>
    public int playSpeed = 2;

    /// <summary>
    /// Is the player being recorded
    /// </summary>
    public bool isRecording = false;

    /// <summary>
    /// Is the recording being played
    /// </summary>
    public bool isPlaying = true;

    /// <summary>
    /// States that are being recorded
    /// </summary>
    private Dictionary<int, PlayerState> _currentStates;

    /// <summary>
    /// States that have been recorded
    /// </summary>
    private Dictionary<int, PlayerState> _recordedStates;

    #endregion

    #region Functions

    /// <summary>
    /// Play one of the players states
    /// </summary>
    /// <param name="aFrame">Frame state is played on</param>
    private void _playRecordedState(int aFrame)
    {
        //If the recorded state contains the frame
        if (_recordedStates.ContainsKey(aFrame))
        {
            //State of the player
            PlayerState playerState = _recordedStates[aFrame];

            //Set its position
            player.transform.position = _recordedStates[aFrame].position;
        }
    }

    /// <summary>
    /// Record the state
    /// </summary>
    private void _record()
    {
        //If the current states have not been created yet, then create it
        if(_currentStates == null)
        {
            _currentStates = new Dictionary<int, PlayerState>();
        }

        //Add the current state of the player
        _currentStates.Add(time, new PlayerState(player.transform.position));
    }

    /// <summary>
    /// Start recording the player
    /// </summary>
    public void startRecording()
    {
        //Set at the beggining and start recording
        time = 0;
        isRecording = true;
        isPlaying = false;
        _currentStates = new Dictionary<int, PlayerState>();
        _recordedStates = new Dictionary<int, PlayerState>();

        //Turn on the players gravity, and collider
        player.rig2d.isKinematic = false;
        player.GetComponent<Collider2D>().enabled = true;

    }

    /// <summary>
    /// Stop recording the player
    /// </summary>
    public void _stopRecording()
    {
        //Stop recording and copy the states to play
        isRecording = false;
        _recordedStates = new Dictionary<int, PlayerState>(_currentStates);
        _currentStates.Clear();
    }

    /// <summary>
    /// Play the recording, either at the beginning and moving forward, or at the end moving background.
    /// </summary>
    /// <param name="atBeginning">Do we start at the beggining or the end?</param>
    private void _playRecording(bool atBeginning)
    {
        //Turn off the players rigid body and collider
        player.rig2d.isKinematic = true;
        player.GetComponent<Collider2D>().enabled = false;

        //If we play at the beginning
        if (atBeginning)
        {
            time = 0;
            rewindTime = false;
        }
        //If we play at the end
        else
        {
            time = _recordedStates.Count - 1;
            rewindTime = true;
        }
                
        //Stop recording and start playing
        isRecording = false;
        isPlaying = true;
    }

    /// <summary>
    /// Reverse back to the begining
    /// </summary>
    public void reverseBackToTheBegining()
    {
        _stopRecording();
        StartCoroutine(_rewindRecording());
    }

    #endregion

    #region Coroutines

    /// <summary>
    /// Rewind the recording to the start
    /// </summary>
    private IEnumerator _rewindRecording()
    {
        //Play the recording
        _playRecording(false);

        //Add the GrayScale effect to the camera
        GrayScale camGrayScale = GrayScale.addToCamera(Camera.main);

        //Delay a bit at the start
        yield return new WaitForSeconds(0.5f);

        //Wait until the player has rewinded to the beginning
        while (time > 0)
        {
            yield return new WaitForFixedUpdate();
        }

        //Make the player controllable
        player.rig2d.isKinematic = false;
        player.GetComponent<Collider2D>().enabled = true;
        rewindTime = false;
        isPlaying = false;
        isRecording = true;

        StartCoroutine(player.IStartCountDownTimer(false));

        //Pause the game
        TimeController.pause();

        //Remove the GrayScale
        Destroy(camGrayScale);
    }

    #endregion

    #region Unity_Functions

    void FixedUpdate()
    {
        //If we are recording
        if (isRecording)
        {
            //Record the player
            _record();
        }

        //If time is being rewinded
        if (rewindTime)
        {
            //If we are playing and not recording
            if (isPlaying && !isRecording)
            {
                //Play the recorded state
                _playRecordedState(time);
                time -= playSpeed;
            }
            else
            {
                //Go back a frame
                time--;
            }

            time = time < 0 ? 0 : time;
        }
        //Else if time is moving forward
        else
        {
            //And if we are playing and not recording
            if (isPlaying && !isRecording)
            {
                //Play the recorded state
                _playRecordedState(time);
                time += playSpeed;
            }
            else
            {
                //Go forward a frame
                time++;
            }
        }
    }
    
    #endregion
}