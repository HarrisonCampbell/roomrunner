﻿using UnityEngine;
using ExtensionClasses.Exceptions;

/// <summary>
/// Health component of the player
/// </summary>
public class PlayerHealth : BaseHealth
{
    #region Variables

    /// <summary>
    /// The players max health
    /// </summary>
    public static new int maxHealth = 5;

    #endregion

    #region Functions

    /// <summary>
    /// Kill the player
    /// </summary>
    private void kill()
    {
        //Lose the game
        GameState.gameManInst.loseLevel();
    }

    /// <summary>
    /// Health is lost
    /// </summary>
    /// <param name="dmg">Amount of damage taken</param>
    public override void takeDamage(int aDmg = 1)
    {
        base.takeDamage(aDmg);
    }

    #endregion

    #region Unity Functions

    void Start()
    {
        //Set the players health to their max health
        base.maxHealth = maxHealth;
        resetHealth();
    }

    void Update()
    {
        //If the player is no longer alive
        if (!isAlive)
        {
            //Kill the player
            kill();
        }
    }

    #endregion
}
