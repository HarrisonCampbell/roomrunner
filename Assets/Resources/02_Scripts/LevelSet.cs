﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

/// <summary>
/// Holds the levels
/// </summary>
public class LevelSet<T>{

    #region Variables

    /// <summary>
    /// Sequence of levels
    /// </summary>
    private List<T> _levels;

    /// <summary>
    /// Sequence of levels
    /// </summary>
    public List<T> levels
    {
        get
        {
            return _levels;
        }
    }

    /// <summary>
    /// The level that is being selected
    /// </summary>
    public int levelSelected = 0;
    
    /// <summary>
    /// Get the level selected
    /// </summary>
    public T value
    {
        get
        {
            return levels[levelSelected];
        }
    }

    /// <summary>
    /// Are we at the beginning of the level set
    /// </summary>
    public bool atTheStart
    {
        get
        {
            return levelSelected == 0;
        }
    }

    /// <summary>
    /// Are we at the end of the level set
    /// </summary>
    public bool atTheEnd
    {
        get
        {
            return levelSelected == levels.Count;
        }
    }

    #endregion

    #region Functions

    /// <summary>
    /// Convert the levels to a string message
    /// </summary>
    /// <returns>Sequence of levels</returns>
    public override string ToString()
    {
        string levelSequence = "";

        for(int i = 0; i < _levels.Count; i++)
        {
            levelSequence += _levels[i].ToString();

            if(i < _levels.Count - 1)
            {
                levelSequence += ",";
            }
        }

        return levelSequence;
    }

    /// <summary>
    /// Shift the selected level down 
    /// </summary>
    public void shiftDown()
    {
        T temp = _levels[levelSelected - 1];
        _levels[levelSelected - 1] = _levels[levelSelected];
        _levels[levelSelected] = temp;
        levelSelected--;
    }

    /// <summary>
    /// Shift the selected level up
    /// </summary>
    public void shiftUp()
    {
        T temp = _levels[levelSelected + 1];
        _levels[levelSelected + 1] = _levels[levelSelected];
        _levels[levelSelected] = temp;
        levelSelected++;
    }

    /// <summary>
    /// Remove the currently selected level
    /// </summary>
    public void remove()
    {
        _levels.RemoveAt(levelSelected);
        levelSelected = Math.Max(0, levelSelected - 1);
    }

    /// <summary>
    /// LevelSet default constructor
    /// </summary>
    public LevelSet()
    {
        _levels = new List<T>();
        levelSelected = 0;
    }

    #endregion

}
